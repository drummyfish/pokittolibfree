This is PokittoLibre SD card. The following is a table of programs it contains.

folder      program(s)           description         author       license
-------------------------------------------------------------------------------
abbaye      l'Abbaye des Morts   platformer game     multiple     GPLv3, CC-BY
ballbust    Ball Bust            game                Xhaku        Unlicense
linez       Linez                tetris clone game   sbmrgd       BSD-3-clause
physics     Physix               physics demo        Pharap       Apache-2.0
pok2048     Pok2048              2048 game           drummyfish   CC0
pokittaire  Pokittaire           solitaire clone     spinal       CC0
raycasting  demo1                raycasting demo     drummyfish   CC0
            demo2                raycasting demo     drummyfish   CC0
            demo3                raycasting demo     drummyfish   CC0
texteds     PokiPad              plain text editor   drummyfish   CC0
            LILIDE               LIL program editor  drummyfish   CC0
uforace     UFO Race             racing game (port)  multiple     LGPL
