This game has been forked (modified) from

https://gitlab.com/HomineLudens/abbaye-des-morts-pokitto

by Drummyfish for PokittoLibre at December 3, 2018.

Original README content follows.

-----

# l'Abbaye des Morts, Pokitto Port

- original source: https://github.com/nevat/abbayedesmorts-gpl
- license: **GPLv3** for code, **CC-BY-3.0** for media
- original game by **LocoMalito**, art by **Gryzor87**
- Pokitto port by **HomineLudens**, **FManga**, **Jonne**, **Drummyfish**, forked (modified) from the original at 23 November 2018

![alt text](graphics/intro1_hc.png)
![alt text](graphics/intro1_lc.png)
