#include "sdsave.h"

const char* pathMap = "abbaye/map.bin";
const char* pathMapSaved = "abbaye/mapsave.bin";
const char* pathEnemies = "abbaye/enemies.bin";
const char* dirAbbaye = "abbaye";

#ifndef POK_SIM
SDFileSystem *sd;
#endif

void initsd()
{
#ifdef POK_SIM
    mkdir(dirAbbaye);
#else
    sd = new SDFileSystem(P0_9, P0_8, P0_6, P0_7, "sd", NC, SDFileSystem::SWITCH_NONE, 25000000 );
    mkdir(dirAbbaye, 0777);
#endif
}

void closesd()
{
#ifndef POK_SIM
    delete sd;
#endif
}

//------------------------------------------------------
//file system wrapper from @FManga
#ifndef POK_SIM
#define fopen fs_fopen
#define fclose fs_fclose
#define fread fs_fread
#define fwrite fs_fwrite
#define fseek fs_fseek

// fopen
FILE * fs_fopen( const char *file, const char *mode )
{
    int flags = O_RDWR;
    if( mode[0] != 'r' )
    {
        flags |= O_CREAT;
        if( mode[0] == 'w' )
            flags |= O_TRUNC;
        else if( mode[0] == 'a' )
            flags |= O_APPEND;
    }
    else if( mode[1] == 0 )
    {
        flags = O_RDONLY;
    }
    return (FILE *) sd->open( file, flags );
}

// fclose
int fs_fclose( FILE *fp )
{
    if( fp )
        ((FATFileHandle*) fp)->close();
    return 0;
}

// fread
unsigned int fs_fread (void *buffer, size_t a, size_t b, FILE *fp)
{
    unsigned int r = ((FATFileHandle *) fp)->read( buffer, a*b );
    if( r == ~0UL ) r = 0;
    return r;
}

// fwrite
unsigned int fs_fwrite (const void *buffer, size_t a, size_t b, FILE *fp)
{
    return ((FATFileHandle *) fp)->write( buffer, a*b );
}

// fseek
int fs_fseek(FILE *fp, long l,int i)
{
    return ((FATFileHandle *) fp)->lseek(l,i-1);
}

#endif


void loadBackgroundImage( const char *img ){
    char path[256];
    sprintf( path, "%s/gfx/%s.gfx", dirAbbaye, img );
    FILE *fp = fopen( path, "rb" );
    if( !fp )
	return;

    !fread( Pokitto::Display::screenbuffer, 1, 0x4BA0, fp );

    fclose(fp);
}


void initrooms()
{
    //Copy map from original to "in play"
    FILE *fpsrc = fopen(pathMap, "rb");
    FILE *fpdst = fopen(pathMapSaved, "wb");
    char line[512];
    size_t r;

    while( (r=fread(line, 1, 512, fpsrc)) ){
	!fwrite( line, 1, r, fpdst );
    }

    fclose(fpsrc);
    fclose(fpdst);
}


void saveroom(uint8_t room,uint16_t roomdata[26][35])
{    
    FILE *filep = fopen(pathMapSaved, "rb+");
    if(filep!=NULL)
    {
	uint32_t offset = uint32_t(room) * 35 * 22;
        fseek( filep, offset*2, SEEK_SET );
	fwrite( roomdata, 2, 22*35, filep );
        fclose(filep);
    }
}

void loadroom(uint8_t room,uint16_t roomdata[26][35])
{
    //Open file
    FILE *filep = fopen(pathMapSaved, "rb+");
    if(filep!=NULL)
    {
	uint32_t offset = uint32_t(room) * 35 * 22;
        fseek( filep, offset*2, SEEK_SET );
	fread( roomdata, 2, 22*35, filep );
        fclose(filep);
    }
}

void loadenemiesroom(uint8_t room,uint16_t enemiesdata[7][15])
{
    //Clean data in case we can't read from file
    for (uint8_t j=0; j<7; j++)
    {
        for (uint8_t k=0; k<15; k++)
        {
            enemiesdata[j][k]=0;
        }
    }

    //Open file
    FILE *filep = fopen(pathEnemies, "r");
    if(filep!=NULL)
    {
        //Jump to room
        int offset=uint32_t(room) * 7 * 15;
        fseek( filep, offset*2, SEEK_SET );
	fread( enemiesdata, 2, 7*15, filep );
        /* Closing file */
        fclose(filep);
    }
}

