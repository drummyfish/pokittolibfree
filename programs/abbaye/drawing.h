/* drawing.h */

#pragma once

# include <stdio.h>
# include <stdlib.h>
# include "structs.h"
# include "Pokitto.h"
# include "sdlpok.h"

const char* msg_levels[] = {"A prayer of hope",
                                 "Tower of the bell",
                                 "Wine supplies",
                                 "Escape !!",
                                 "Death is close",
                                 "Abandoned church",
                                 "The Altar",
                                 "Hangman tree",
                                 "Pestilent beast",
                                 "Cave of illusion",
                                 "Plague Ruins",
                                 "Catacombs",
                                 "Hidden garden",
                                 "Gloomy tunnels",
                                 "Lake of despair",
                                 "The wheel of faith",
                                 "Banquet of death",
                                 "Underground river",
                                 "Unexpected gate",
                                 "Evil church",
                                 "Tortured souls",
                                 "Ashes to ashes",
                                 "Satan !!"
                                };

