#!/bin/bash

# Build all programs and make an SD card archive.
# author: Miloslav Ciz
# license: CC0 1.0

if [ "$#" -ne 1 ]; then
  echo "I need a path to GNU ARM Toolchain bin folder as a parameter."
  exit 1
fi

ARCHIVE_NAME="sdcard.zip"
GTC_PATH=$1

cd ../PokittoLib
POKITTOLIB_PATH=$(pwd)
cd ../programs

function build { # (program, build)
  echo "building $1 ($build)"

  cd "$1"

  rm ./PokittoLib
  ln -s "$POKITTOLIB_PATH" ./PokittoLib

  rm ./gtc
  ln -s "$GTC_PATH" ./gtc

  make BUILD="$2"

  if [ $? -ne 0 ]; then
    echo "ERROR!"
    exit 1
  fi

  rm ./PokittoLib
  rm ./gtc

  cd ..

  cp "./$1/BUILD/firmware.bin" "tmp/${1}${2}.bin"

  rm -rd "./$1/BUILD"
}

rm $ARCHIVE_NAME

mkdir tmp

build "abbaye"
cp -r abbaye/sdcard/ tmp/abbaye
build "ballbust"
build "linez"
build "physics"
build "pok2048"
build "pokittaire"
build "raycasting" "demo1"
build "raycasting" "demo2"
build "raycasting" "demo3"
build "texteds" "pokipad"
build "texteds" "lilide"
build "uforace"

echo "creating the archive"

cp sd_readme.txt tmp/readme.txt

rm "$ARCHIVE_NAME"

cd tmp

zip -r "../$ARCHIVE_NAME" ./*

cd ..

rm -rf tmp
