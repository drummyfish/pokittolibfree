/**
  Advanced onscreen keyboard for Pokitto and related stuff module.
  Author: Miloslav Ciz
  License: CC0

  If you just want to read something from the user, use popUpKeyboard(...)
  function. For advanced use of the keyboard take a look at the classes. 
*/

#ifndef POKITTO_KEYBOARD_H
#define POKITTO_KEYBOARD_H

#include "Pokitto.h"
#include <cstdint>

#define POK Pokitto::Core

#ifndef LAYOUT
#define LAYOUT 2 // 0: ABC, 1: QWERTY, 2:QWERTZ
#endif

using namespace std;

enum KeyboardAction: uint8_t
{
  UpperA,
  UpperB,
  UpperC,
  UpperD,
  UpperE,
  UpperF,
  UpperG,
  UpperH,
  UpperI,
  UpperJ,
  UpperK,
  UpperL,
  UpperM,
  UpperN,
  UpperO,
  UpperP,
  UpperQ,
  UpperR,
  UpperS,
  UpperT,
  UpperU,
  UpperV,
  UpperW,
  UpperX,
  UpperY,
  UpperZ,
  LowerA,
  LowerB,
  LowerC,
  LowerD,
  LowerE,
  LowerF,
  LowerG,
  LowerH,
  LowerI,
  LowerJ,
  LowerK,
  LowerL,
  LowerM,
  LowerN,
  LowerO,
  LowerP,
  LowerQ,
  LowerR,
  LowerS,
  LowerT,
  LowerU,
  LowerV,
  LowerW,
  LowerX,
  LowerY,
  LowerZ,
  Num0,
  Num1,
  Num2,
  Num3,
  Num4,
  Num5,
  Num6,
  Num7,
  Num8,
  Num9,
  Period,
  Comma,
  Semicolon,
  Colon,
  ExclamationMark,
  QuestionMark,
  Quotes,
  Apostrophe,
  SingleQuote,
  Caret,
  BracketLeftRound,
  BracketRightRound,
  BracketLeftSquare,
  BracketRightSquare,
  BracketLeftCurly,
  BracketRightCurly,
  LessThan,
  GreaterThan,
  Equals,
  Minus,
  Plus,
  Asterisk,
  Tilde,
  Slash,
  BackSlash,
  Underscore,
  Bar,
  Sharp,
  Dollar,
  Percent,
  Ampersand,
  At,
  Space,

  _SPECIAL, // marks the start of special actions

  Tab,
  Enter,
  Backspace,
  Delete,
  Cancel,
  Shift,
  Control,
  ArrowLeft,
  ArrowRight,
  ArrowUp,
  ArrowDown,
  SwitchPage,
  None
};

const char actionChars[] =    // for non-special actions only
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,;:!?\"'`^()[]{}<>=-+*~/\\_|#$%&@  ";

const uint8_t actionIcons[] = // for special actions only
{
  8,5,11,

  0x00,
  0x90,
  0xF8,
  0x90,
  0x00,

  0x08,
  0x08,
  0x48,
  0xF8,
  0x40,

  0x20,
  0x78,
  0xC0,
  0x78,
  0x20,

  0x20,
  0xF0,
  0x18,
  0xF0,
  0x20,

  0xD8,
  0x70,
  0x20,
  0x70,
  0xD8,

  0x40,
  0xE0,
  0x00,
  0xE0,
  0x40,

  0x20,
  0x50,
  0x88,
  0xF8,
  0x00,

  0x20,
  0x60,
  0xE0,
  0x60,
  0x20,

  0x20,
  0x30,
  0x38,
  0x30,
  0x20,

  0x20,
  0x70,
  0xF8,
  0x00,
  0x00,

  0x00,
  0x00,
  0xF8,
  0x70,
  0x20,

  0xE0,
  0xC8,
  0x98,
  0x38,
  0x00,

  0,0,0,0,0,0,0,0
};

/**
  Handles an onscreen keyboard, which is a board of buttons that the user
  can press. Drawing and user-interaction is handled, but the keyboard doesn't
  know or care about what the key presses mean and do. For that, use keyboard
  handlers.
*/
class Keyboard
{
protected:
  KeyboardAction        currentAction;

  uint8_t               colorSpecial;
  uint8_t               colorText;
  uint8_t               colorButton;
  uint8_t               colorBackground;
  uint8_t               colorSelection;
  uint8_t               colorDetail;
  uint8_t               colorInvisible;

  inline bool buttonTriggered(int16_t button)
  {
    // TODO FIXME: once getFrameRate is added to PokittoLib, change the hardcoded value to a one computed from FPS
    return POK::buttons.pressed(button) || (POK::buttons.timeHeld(button) > 6 && POK::buttons.repeat(button,2));
  }

public:
  const static int16_t KEYS_X = 15;
  const static int16_t KEYS_Y = 4;
  const static int16_t KEYS_TOTAL = KEYS_X * KEYS_Y;
  const static int16_t BUTTON_SIZE = 6;
  const static int16_t WIDTH = KEYS_X * (BUTTON_SIZE + 1) + 6;
  const static int16_t HEIGHT = KEYS_Y * (BUTTON_SIZE + 1) + 1;

  uint16_t              selectionX;
  uint16_t              selectionY;
  bool                  hasFocus;

  #define A(x) KeyboardAction::x
  #define AU(x) KeyboardAction::Upper##x
  #define AL(x) KeyboardAction::Lower##x
  #define AN(x) KeyboardAction::Num##x

  constexpr static KeyboardAction layoutABC[KEYS_TOTAL * 2] =
  {
    // page 1
    A(Cancel),  AN(0), AN(1), AN(2), AN(3), AN(4), AN(5), AN(6),         AN(7),    AN(8),          AN(9),         A(Backspace), A(Delete),     A(LessThan),  A(GreaterThan),
    A(Tab),     AU(A), AU(B), AU(C), AU(D), AU(E), AU(F), AU(G),         AU(H),    AU(I),          AU(J),         A(Enter),     A(Ampersand),  A(Plus),      A(Minus),
    A(Shift),   AU(K), AU(L), AU(M), AU(N), AU(O), AU(P), AU(Q),         AU(R),    AU(S),          AU(T),         A(ArrowUp),   A(At),         A(Equals),    A(Asterisk),
    A(Control), AU(U), AU(V), AU(W), AU(X), AU(Y), AU(Z), A(Underscore), A(Space), A(SwitchPage),  A(ArrowLeft),  A(ArrowDown), A(ArrowRight), A(Slash),     A(BackSlash),

    // page 2
    A(Cancel),  A(Period), A(Comma), A(Semicolon), A(Colon), A(ExclamationMark), A(QuestionMark), A(Quotes), A(Apostrophe), A(SingleQuote), A(Caret),      A(Backspace), A(Delete),     A(BracketLeftRound),  A(BracketRightRound),
    A(Tab),     AL(A),     AL(B),    AL(C),        AL(D),    AL(E),              AL(F),           AL(G),     AL(H),         AL(I),          AL(J),         A(Enter),     A(Percent),    A(BracketLeftSquare), A(BracketRightSquare),
    A(Shift),   AL(K),     AL(L),    AL(M),        AL(N),    AL(O),              AL(P),           AL(Q),     AL(R),         AL(S),          AL(T),         A(ArrowUp),   A(Dollar),     A(BracketLeftCurly),  A(BracketRightCurly),
    A(Control), AL(U),     AL(V),    AL(W),        AL(X),    AL(Y),              AL(Z),           A(Space),  A(Sharp),      A(SwitchPage),  A(ArrowLeft),  A(ArrowDown), A(ArrowRight), A(Tilde),             A(Bar)
  };

  constexpr static KeyboardAction layoutQWERTY[KEYS_TOTAL * 2] =
  {
    // page1
    A(Cancel),  AN(0), AN(1), AN(2), AN(3), AN(4), AN(5), AN(6), AN(7),    AN(8),          AN(9),         A(Backspace), A(Delete),     A(LessThan),  A(GreaterThan),
    A(Tab),     AU(Q), AU(W), AU(E), AU(R), AU(T), AU(Y), AU(U), AU(I),    AU(O),          AU(P),         A(Enter),     A(Ampersand),  A(Plus),      A(Minus),
    A(Shift),   AU(A), AU(S), AU(D), AU(F), AU(G), AU(H), AU(J), AU(K),    AU(L),          A(Underscore), A(ArrowUp),   A(At),         A(Equals),    A(Asterisk),
    A(Control), AU(Z), AU(X), AU(C), AU(V), AU(B), AU(N), AU(M), A(Space), A(SwitchPage),  A(ArrowLeft),  A(ArrowDown), A(ArrowRight), A(Slash),     A(BackSlash),

    // page2
    A(Cancel),  A(Period), A(Comma), A(Semicolon), A(Colon), A(ExclamationMark), A(QuestionMark), A(Quotes), A(Apostrophe), A(SingleQuote), A(Caret),      A(Backspace), A(Delete),     A(BracketLeftRound),  A(BracketRightRound),
    A(Tab),     AL(Q),     AL(W),    AL(E),        AL(R),    AL(T),              AL(Y),           AL(U),     AL(I),         AL(O),          AL(P),         A(Enter),     A(Percent),    A(BracketLeftSquare), A(BracketRightSquare),
    A(Shift),   AL(A),     AL(S),    AL(D),        AL(F),    AL(G),              AL(H),           AL(J),     AL(K),         AL(L),          A(Sharp),      A(ArrowUp),   A(Dollar),     A(BracketLeftCurly),  A(BracketRightCurly),
    A(Control), AL(Z),     AL(X),    AL(C),        AL(V),    AL(B),              AL(N),           AL(M),     A(Space),      A(SwitchPage),  A(ArrowLeft),  A(ArrowDown), A(ArrowRight), A(Tilde),             A(Bar)
  };

  constexpr static KeyboardAction layoutQWERTZ[KEYS_TOTAL * 2] =
  {
    // page1
    A(Cancel),  AN(0), AN(1), AN(2), AN(3), AN(4), AN(5), AN(6), AN(7),    AN(8),          AN(9),         A(Backspace), A(Delete),     A(LessThan),  A(GreaterThan),
    A(Tab),     AU(Q), AU(W), AU(E), AU(R), AU(T), AU(Z), AU(U), AU(I),    AU(O),          AU(P),         A(Enter),     A(Ampersand),  A(Plus),      A(Minus),
    A(Shift),   AU(A), AU(S), AU(D), AU(F), AU(G), AU(H), AU(J), AU(K),    AU(L),          A(Underscore), A(ArrowUp),   A(At),         A(Equals),    A(Asterisk),
    A(Control), AU(Y), AU(X), AU(C), AU(V), AU(B), AU(N), AU(M), A(Space), A(SwitchPage),  A(ArrowLeft),  A(ArrowDown), A(ArrowRight), A(Slash),     A(BackSlash),

    // page2
    A(Cancel),  A(Period), A(Comma), A(Semicolon), A(Colon), A(ExclamationMark), A(QuestionMark), A(Quotes), A(Apostrophe), A(SingleQuote), A(Caret),      A(Backspace), A(Delete),     A(BracketLeftRound),  A(BracketRightRound),
    A(Tab),     AL(Q),     AL(W),    AL(E),        AL(R),    AL(T),              AL(Z),           AL(U),     AL(I),         AL(O),          AL(P),         A(Enter),     A(Percent),    A(BracketLeftSquare), A(BracketRightSquare),
    A(Shift),   AL(A),     AL(S),    AL(D),        AL(F),    AL(G),              AL(H),           AL(J),     AL(K),         AL(L),          A(Sharp),      A(ArrowUp),   A(Dollar),     A(BracketLeftCurly),  A(BracketRightCurly),
    A(Control), AL(Y),     AL(X),    AL(C),        AL(V),    AL(B),              AL(N),           AL(M),     A(Space),      A(SwitchPage),  A(ArrowLeft),  A(ArrowDown), A(ArrowRight), A(Tilde),             A(Bar)
  };

  #undef A
  #undef AU
  #undef AL
  #undef AN

  const KeyboardAction *layout;

  int8_t page;

  Keyboard()
  {
    this->selectionX = 0;
    this->selectionY = 0;
    this->hasFocus = true;
    this->currentAction = KeyboardAction::None;

    this->colorSpecial = 7;
    this->colorText = 8;
    this->colorButton = 9;
    this->colorBackground = 1;
    this->colorSelection = 0;
    this->colorDetail = 3;
    this->colorInvisible = 17;

    this->layout = Keyboard::layoutQWERTY;
    this->page = 0;
  }

  void setLayout(const KeyboardAction *layout)
  {
    this->layout = layout;
  }

  void setColors(
    uint8_t text,
    uint8_t button,
    uint8_t background,
    uint8_t select,
    uint8_t special,
    uint8_t detail)
  {
    this->colorText = text;
    this->colorButton = button;
    this->colorBackground = background;
    this->colorSelection = select;
    this->colorSpecial = special;
    this->colorDetail = detail;
 
    this->colorInvisible = 17;

    for (uint16_t i = 0; i < 256; ++i)
      if (i != text && i != button && i != background && i != select && i != special && i != detail)
      {
        this->colorInvisible = i;
        break;
      }
  }

  void getColors(
    uint8_t *text,
    uint8_t *button,
    uint8_t *background,
    uint8_t *select,
    uint8_t *special,
    uint8_t *detail)
  {
    *text = this->colorText;
    *button = this->colorButton;
    *background = this->colorBackground;
    *select = this->colorSelection;
    *special = this->colorSpecial;
    *detail = this->colorDetail;
  }

  KeyboardAction getAction()
  {
    return this->currentAction;
  }

  void update()
  {
    this->currentAction = KeyboardAction::None;

    if (!this->hasFocus)
      return;

    bool bDown = POK::buttons.timeHeld(BTN_B) >= 1;
    bool cDown = POK::buttons.timeHeld(BTN_C) >= 1;

    if (bDown)
    {
      // shortcuts with B held

      if (this->buttonTriggered(BTN_RIGHT))
        this->currentAction = KeyboardAction::ArrowRight;
      else if (this->buttonTriggered(BTN_LEFT))
        this->currentAction = KeyboardAction::ArrowLeft;
      else if (this->buttonTriggered(BTN_UP))
        this->currentAction = KeyboardAction::ArrowUp;
      else if (this->buttonTriggered(BTN_DOWN))
        this->currentAction = KeyboardAction::ArrowDown;
      else if (this->buttonTriggered(BTN_A))
        this->currentAction = KeyboardAction::Delete;
      else if (this->buttonTriggered(BTN_C))
        this->currentAction = KeyboardAction::Backspace;
    }
    else if (cDown)
    {
      // shortcuts with C held

      if (this->buttonTriggered(BTN_UP))
        this->currentAction = KeyboardAction::Space;
      else if (this->buttonTriggered(BTN_DOWN))
        this->currentAction = KeyboardAction::Shift;
      else if (this->buttonTriggered(BTN_A))
        this->page = this->page == 0 ? 1 : 0;
    }
    else
    {
      if (this->buttonTriggered(BTN_RIGHT))
        this->selectionX = (this->selectionX + 1) % Keyboard::KEYS_X; 
      else if (this->buttonTriggered(BTN_LEFT))
        this->selectionX = this->selectionX > 0 ? this->selectionX - 1 : Keyboard::KEYS_X - 1;

      if (this->buttonTriggered(BTN_DOWN))
        this->selectionY += this->selectionY < Keyboard::KEYS_Y - 1 ? 1 : 0;
      else if (this->buttonTriggered(BTN_UP))
        this->selectionY -= this->selectionY > 0 ? 1 : 0; 

      if (this->buttonTriggered(BTN_A))
      {
        this->currentAction = this->layout[this->page * Keyboard::KEYS_TOTAL + this->selectionY * Keyboard::KEYS_X + this->selectionX];
   
        if (this->currentAction == KeyboardAction::SwitchPage)
        {
          this->page = this->page == 0 ? 1 : 0;
          this->currentAction = KeyboardAction::None;
        }
      }
    }
  }

  void draw(int16_t x, int16_t y)
  {
    uint16_t backupColor = POK::display.color;
    uint16_t backupBgColor = POK::display.bgcolor;
    const unsigned char *fontBackup = POK::display.font;

    POK::display.setFont(fontTiny);
    POK::display.setColor(this->colorBackground);
    POK::display.fillRect(x,y,Keyboard::WIDTH,Keyboard::HEIGHT);
    
    POK::display.setColor(this->colorText);
    POK::display.drawFastHLine(x,y,Keyboard::WIDTH);

    POK::display.setColor(this->colorDetail);
    POK::display.drawFastVLine(x,y,Keyboard::HEIGHT);
    POK::display.drawFastVLine(Keyboard::WIDTH - 2,y,Keyboard::HEIGHT);

    y += 2;

    POK::display.bgcolor = POK::display.invisiblecolor;

    for (int16_t keyY = 0; keyY < Keyboard::KEYS_Y; ++keyY)
    {
      int16_t drawY = y + keyY * (Keyboard::BUTTON_SIZE + 1);

      for (int16_t keyX = 0; keyX < Keyboard::KEYS_X; ++keyX)
      {
        int16_t drawX = x + 3 + keyX * (Keyboard::BUTTON_SIZE + 1);

        KeyboardAction action = this->layout[this->page * Keyboard::KEYS_TOTAL + keyY * Keyboard::KEYS_X + keyX];
        
        bool selected = this->hasFocus && keyX == this->selectionX && keyY == this->selectionY;

        if (!selected)
        {
          POK::display.setColor(this->colorButton);
          POK::display.fillRect(drawX, drawY, Keyboard::BUTTON_SIZE, Keyboard::BUTTON_SIZE - 1);

          POK::display.setColor(this->colorDetail);

          if (keyX < Keyboard::KEYS_X - 1)
            POK::display.drawFastVLine(drawX + Keyboard::BUTTON_SIZE,drawY,Keyboard::BUTTON_SIZE - 1);

          if (keyY < Keyboard::KEYS_Y - 1)
            POK::display.drawFastHLine(drawX,drawY + Keyboard::BUTTON_SIZE,Keyboard::BUTTON_SIZE);

          POK::display.setColor(action <= KeyboardAction::_SPECIAL ? this->colorText : this->colorSpecial);
        }
        else
        {
          POK::display.setColor(this->colorBackground);
          POK::display.fillRect(drawX - 1, drawY - 1, Keyboard::BUTTON_SIZE + 2, Keyboard::BUTTON_SIZE + 1);
          POK::display.setColor(this->colorSelection);
        }

        if (action <= KeyboardAction::_SPECIAL)
        {
          POK::display.setCursor(drawX, drawY - 1);
          POK::display.print(actionChars[action]);
        }
        else
        {
          POK::display.drawMonoBitmap(drawX,drawY,actionIcons,action - KeyboardAction::_SPECIAL - 1);
        }

        if (selected)
        {
          POK::display.setColor(this->colorSelection);
          POK::display.drawRect(drawX - 2,drawY - 2,Keyboard::BUTTON_SIZE + 2,Keyboard::BUTTON_SIZE + 2);
        }
      }
    }

    POK::display.color = backupColor;
    POK::display.bgcolor = backupBgColor;
    POK::display.setFont(fontBackup);
  }
};

constexpr KeyboardAction Keyboard::layoutABC[];
constexpr KeyboardAction Keyboard::layoutQWERTZ[];
constexpr KeyboardAction Keyboard::layoutQWERTY[];

class KeyboardHandler
{
protected:
  Keyboard *keyboard;

public:
  KeyboardHandler(Keyboard *keyboard)
  {
    this->keyboard = keyboard;
  }

  void update()
  {
    this->keyboard->update();

    KeyboardAction action = this->keyboard->getAction();

    if (action != KeyboardAction::None)
      this->handleKeyboardAction(action);
  }

  virtual void handleKeyboardAction(KeyboardAction action) = 0;
};

/**
  Handles text manipulation using keyboard. This class implements basic
  functions, but is supposed to be subclassed and further specialized for
  any particular application.
*/
template <typename T>
class TextKeyboardHandler: public KeyboardHandler
{
protected:
  T       *textBuffer;
  size_t  textBufferLen;
  size_t  cursorPos;

public:
  TextKeyboardHandler(Keyboard *keyboard, T *textBuffer, size_t textBufferLen): KeyboardHandler(keyboard)
  {
    this->textBuffer = textBuffer;
    this->textBufferLen = textBufferLen;
    this->cursorPos = 0;
  }

  size_t getCursorPosition()
  {
    return this->cursorPos;
  }

  void setCursorPosition(size_t position)
  {
    this->cursorPos = position;
  }

  virtual void put(T character)
  {
    if (this->cursorPos < this->textBufferLen - 2)
    {
      for (size_t i = this->textBufferLen - 2; i > this->cursorPos; --i)
        this->textBuffer[i] = this->textBuffer[i - 1];

      this->textBuffer[this->cursorPos] = character;
      this->textBuffer[this->textBufferLen - 1] = 0; // just to be sure
      this->cursorPos++;

    }
  }

  void performDelete()
  {
    if ((this->cursorPos >= this->textBufferLen - 1) || this->textBuffer[this->cursorPos] == 0)
      return;

    for (size_t i = this->cursorPos; i < this->textBufferLen - 1; ++i)
      this->textBuffer[i] = this->textBuffer[i + 1];
  }

  void performBackspace()
  {
    if (this->cursorPos < 1)
      return;

    this->cursorPos--;
    this->performDelete();
  }

  virtual void handleKeyboardAction(KeyboardAction action) override
  {
    if (action < KeyboardAction::_SPECIAL)
      this->put(actionChars[action]);
    else if (action == KeyboardAction::Backspace)
      this->performBackspace();
    else if (action == KeyboardAction::Delete)
      this->performDelete(); 
  }
};

/**
  Handles loading a short text answer from the user using an onscreen keyboard,
  including the GUI and interaction.
*/
template <typename T>
class PopUpAnswerKeyboardHandler: public TextKeyboardHandler<T>
{
public:
  static const uint8_t EXTRA_HEIGHT = 16;

protected:
  bool confirmed;
  bool cancelled;
  const char *question;

  uint8_t c1, c2, c3, c4, c5, c6; // colors

  void draw(int16_t x, int16_t y)
  {
    POK::display.setFont(fontTiny);
    uint16_t keyboardY = y + EXTRA_HEIGHT;

    POK::display.setColor(this->c3);
    POK::display.fillRect(x,keyboardY - EXTRA_HEIGHT,Keyboard::WIDTH,EXTRA_HEIGHT);
    POK::display.setColor(this->c1);
    POK::display.drawRect(x,keyboardY - EXTRA_HEIGHT,Keyboard::WIDTH,EXTRA_HEIGHT);

    POK::display.setColor(this->c5);
    POK::display.bgcolor = this->c3;

    POK::display.setCursor(x + 2,keyboardY - EXTRA_HEIGHT + 1);
    POK::display.print(this->question);

    POK::display.setColor(this->c1);
    POK::display.setCursor(x + 2,keyboardY - EXTRA_HEIGHT / 2);
    POK::display.print(this->textBuffer);

    if ((POK::frameCount >> 2) % 2 == 0)
    {
      POK::display.setColor(this->c5);
      POK::display.print('_'); // cursor
    }

    this->keyboard->draw(x,keyboardY);
  }

public:
  PopUpAnswerKeyboardHandler(Keyboard *keyboard, T *textBuffer, size_t textBufferLen):
    TextKeyboardHandler<T>(keyboard, textBuffer, textBufferLen)
  {
    this->confirmed = false;
    this->cancelled = false;
  }

  virtual void handleKeyboardAction(KeyboardAction action) override
  {
    TextKeyboardHandler<T>::handleKeyboardAction(action);

    if (action == KeyboardAction::Enter)
      this->confirmed = true;
    else if (action == KeyboardAction::Cancel)
      this->cancelled = true;
  }

  bool promptAnswer(const char *question, int16_t x, int16_t y)
  {
    bool persistenceBackup = POK::display.persistence;
    uint8_t colorBackup = POK::display.color;
    uint8_t backgroundBackup = POK::display.bgcolor;
    bool focusBackup = this->keyboard->hasFocus;
    const unsigned char *fontBackup = POK::display.font;

    POK::display.persistence = true;

    this->question = question;

    this->keyboard->getColors(&this->c1,&this->c2,&this->c3,&this->c4,&this->c5,&this->c6);

    this->confirmed = false;
    this->cancelled = false;

    this->cursorPos = 0;

    while (this->cursorPos < this->textBufferLen && this->textBuffer[this->cursorPos] != 0)
      this->cursorPos++;

    this->keyboard->hasFocus = true;

    while (POK::isRunning())
    {
      if (POK::update())
      {
        this->update();
        this->draw(x,y);

        if (this->confirmed || this->cancelled || POK::buttons.pressed(BTN_B))
          break;
      }
    }

    this->keyboard->hasFocus = focusBackup;
    POK::display.persistence = persistenceBackup;
    POK::display.color = colorBackup;
    POK::display.bgcolor = backgroundBackup;
    POK::display.setFont(fontBackup);

    return this->confirmed;
  }
};

/**
  Handles editing of long texts with newlines in an advanced way (e.g.
  PC-like cursor movement, text selection etc.). This class doesn't handle
  drawing such text.
*/
template <typename T>
class MultiLineKeyboardHandler: public TextKeyboardHandler<T>
{
protected:
  size_t cursorRow;
  size_t cursorColumn;
  size_t desiredColumn;
  size_t tabsBeforeCursor;
  T      newLine;

  bool   selecting;
  size_t selectionStart;

  void updateCursorColumnRow()
  {
    this->cursorColumn = 0;
    this->cursorRow = 0;
    this->tabsBeforeCursor = 0;

    for (size_t i = 0; i < this->cursorPos; ++i)
    {
      this->cursorColumn++;

      if (this->textBuffer[i] == this->newLine)
      {
        this->cursorColumn = 0;
        this->cursorRow++;
        this->tabsBeforeCursor = 0;
      }
      else if (this->textBuffer[i] == '\t')
      {
        this->tabsBeforeCursor++;
      }
    }
  }

public:
  MultiLineKeyboardHandler(Keyboard *keyboard, T *textBuffer, size_t textBufferLen):
    TextKeyboardHandler<T>(keyboard, textBuffer, textBufferLen)
  {
    this->cursorRow = 0;
    this->cursorColumn = 0;
    this->desiredColumn = 0;
    this->newLine = '\n';     // for T other than char you might want to change
    this->selecting = false;
    this->selectionStart = 0;
    this->tabsBeforeCursor = 0;
  }

  virtual void put(T character) override
  {
    TextKeyboardHandler<T>::put(character);
    this->updateCursorColumnRow();
  }

  void moveCursor(KeyboardAction action)
  {
    switch (action)
    {
      case KeyboardAction::ArrowLeft:
        this->cursorPos -= this->cursorPos > 0 ? 1 : 0;
        break;

      case KeyboardAction::ArrowRight:
        this->cursorPos += this->textBuffer[this->cursorPos] != 0 ? 1 : 0;
        break;

      case KeyboardAction::ArrowUp:
      {
        if (this->cursorRow < 1)
          break;

        while (this->cursorPos >= 0)
        {
          this->cursorPos--;

          if (this->textBuffer[this->cursorPos] == this->newLine)
            break;
        }

        while (this->cursorPos > 0 && this->textBuffer[this->cursorPos - 1] != '\n')
          this->cursorPos--;

        for (size_t i = 0; i < this->desiredColumn; ++i)
        {
          if (this->textBuffer[this->cursorPos] == this->newLine)
            break;

          this->cursorPos++; 
        }

        break;
      }

      case KeyboardAction::ArrowDown:
      {
        size_t pos = this->cursorPos;

        while (this->textBuffer[pos] != this->newLine && this->textBuffer[pos] != 0)
          pos++;

        if (this->textBuffer[pos] == 0)
          break;

        pos++;

        for (size_t i = 0; i < this->desiredColumn; ++i)
        {
          if (this->textBuffer[pos] == this->newLine || this->textBuffer[pos] == 0)
            break;

          pos++; 
        }

        this->cursorPos = pos;

        break;
      }

      default: break;
    }
  }

  void select(size_t selectionStart, size_t selectionEnd)
  {
    this->selecting = true;
    this->selectionStart = selectionStart;
    this->cursorPos = selectionEnd;
    this->updateCursorColumnRow();
  }

  void unselect()
  {
    this->selecting = false;
  }

  void deleteSelection()
  {
    if (!this->selecting)
      return;

    size_t selectionFrom = min(this->selectionStart,this->cursorPos);
    size_t selectionTo = max(this->selectionStart,this->cursorPos);

    size_t i = selectionFrom;
    size_t j = selectionTo;

    while (j < this->textBufferLen)
    {
      this->textBuffer[i] = this->textBuffer[j];
      ++i;
      ++j;
    }

    this->cursorPos = selectionFrom;
    this->selecting = false;
  }

  virtual void handleKeyboardAction(KeyboardAction action) override
  {
    T c = 0;

    if (action < KeyboardAction::_SPECIAL)
      c = actionChars[action];
    else if (action == KeyboardAction::Enter)
      c = this->newLine;

    if (c != 0)
    {
      this->deleteSelection();
      this->put(c);
    }

    else if (action == KeyboardAction::Backspace)
    {
      if (this->selecting)
        this->deleteSelection();
      else
        this->performBackspace();
    }
    else if (action == KeyboardAction::Delete)
    {
      if (this->selecting)
        this->deleteSelection();
      else
        this->performDelete();
    }
    else if (action >= KeyboardAction::ArrowLeft && action <= KeyboardAction::ArrowDown)
    {
      this->moveCursor(action);
    }
    else if (action == KeyboardAction::Shift)
    {
      this->selecting = !this->selecting;
      this->selectionStart = this->cursorPos;
    }
    else if (action == KeyboardAction::Tab)
    {
      this->put('\t');
    }

    this->updateCursorColumnRow();

    if (action == KeyboardAction::ArrowLeft || action == KeyboardAction::ArrowRight)
      this->desiredColumn = this->cursorColumn;
  }

  size_t getCursorColumn()
  {
    return this->cursorColumn;
  }
  
  size_t getCursorRow()
  {
    return this->cursorRow;
  }

  size_t getTabsBeforeCursor()
  {
    return this->tabsBeforeCursor;
  }

  size_t getSelectionStart()
  {
    return this->selecting ? this->selectionStart : this->cursorPos;
  }

  bool isSelecting()
  {
    return this->selecting;
  }
};

/**
  Handles drawing a long, multiline text, with scrolling.
*/
class TextView
{
protected:
  const char    *textBuffer;
  const uint8_t *font;
  uint8_t        colorBackground;
  uint8_t        colorText;
  uint8_t        colorSelectionBackground;
  uint8_t        colorSelectionText;

  uint8_t        width;
  uint8_t        height;

  uint8_t        widthChars;
  uint8_t        heightChars;

  uint8_t        charWidth;
  uint8_t        charHeight;

  void inline drawCursor(uint16_t x, uint16_t y, bool isSelecting)
  {
    if (isSelecting)
    {
      POK::display.setColor(this->colorSelectionBackground);
      POK::display.drawFastVLine(x,y,this->charHeight);
    }

    if ((POK::frameCount >> 2) % 4 == 0)
      return;

    POK::display.setColor(this->colorText);
    POK::display.drawFastVLine(x,y,this->charHeight);
  }

public:
  uint8_t tabWidth;

  TextView(const char *textBuffer, const uint8_t *font, uint8_t width, uint8_t height)
  {
    this->textBuffer = textBuffer;
    this->font = font;

    this->width = width;
    this->height = height;

    this->charWidth = font[0] + 1;
    this->charHeight = font[1] + 1;

    this->widthChars = this->width / this->charWidth;
    this->heightChars = this->height / this->charHeight;

    this->colorBackground = 1;
    this->colorText = 0;
    this->colorSelectionBackground = 2;
    this->colorSelectionText = 1;

    this->tabWidth = 4;
  }

  void setColors(uint8_t text, uint8_t background, uint8_t selectionText, uint8_t selectionBackground)
  {
    this->colorText = text;
    this->colorBackground = background;
    this->colorSelectionText = selectionText;
    this->colorSelectionBackground = selectionBackground;
  }

  uint8_t getWidthChars()
  {
    return this->widthChars;
  }

  uint8_t getHeightChars()
  {
    return this->heightChars;
  }

  void draw(int16_t x, int16_t y, size_t cursorPos, uint8_t linesBefore, uint8_t column = 0, bool isSelecting = false, size_t selectionStart = 0)
  {
    uint8_t colorBackup = POK::display.color;
    uint8_t backgroundBackup = POK::display.bgcolor;
    const uint8_t *fontBackup = POK::display.font;

    POK::display.setColor(this->colorBackground);
    POK::display.fillRect(x,y,this->width,this->height);
    POK::display.setFont(this->font);

    size_t pos = cursorPos;
    size_t cursorOffset = 0;

    size_t selectionFrom = 0;
    size_t selectionTo = 0;

    if (isSelecting)
    {
      selectionFrom = min(selectionStart,cursorPos);
      selectionTo = max(selectionStart,cursorPos);
    }

    // go back given number of lines

    while (pos > 0 && linesBefore > 0)
    {
      do // go to a nearest previous line end (or string start)
      {
        --pos;
        ++cursorOffset;
      }
      while (pos > 0 && this->textBuffer[pos] != '\n');

      if (pos < 1 || linesBefore < 2)
        break;

      --linesBefore;
    }

    // go to the line start

    while (pos > 0 && this->textBuffer[pos - 1] != '\n')
    {
      --pos;
      ++cursorOffset;
    }

    uint16_t drawY = y;

    // draw rows

    for (uint8_t row = 0; row < this->heightChars; ++row)
    {
      uint16_t drawX = x;

      // get to the start of the view

      for (uint8_t i = 0; i < column; ++i)
      {
        if (this->textBuffer[pos] == '\n' || this->textBuffer[pos] == 0)
          break;

        ++pos;
        --cursorOffset;
      }

      // write the chars in the view row

      for (uint8_t col = 0; col < this->widthChars; ++col)
      {
        char c = this->textBuffer[pos];
        bool cursor = cursorOffset == 0;

        if (c == '\n' || c == 0)
        {
          if (cursor)
            drawCursor(drawX,drawY,isSelecting);

          break;
        }

        ++pos;
        --cursorOffset;

        uint8_t repeats = 1; // for tabs

        if (c == '\t')
        {
          size_t globalColumn = column + col;
          size_t nextTabStop = (globalColumn / this->tabWidth + 1) * this->tabWidth;
          repeats = max(1,nextTabStop - globalColumn);
          c = ' ';
        }

        if (pos <= selectionFrom || pos > selectionTo)
        {
          POK::display.setColor(this->colorText);
          POK::display.bgcolor = this->colorBackground;
        }
        else
        {
          POK::display.setColor(this->colorSelectionBackground);
          POK::display.fillRect(drawX,drawY,repeats * this->charWidth,this->charHeight);
          POK::display.setColor(this->colorSelectionText);
          POK::display.bgcolor = this->colorSelectionBackground;
        }

        POK::display.setCursor(drawX + 1,drawY);

        for (uint8_t i = 0; i < repeats; ++i)
          POK::display.print(c);
        
        if (cursor)
          drawCursor(drawX,drawY,isSelecting);

        drawX += repeats * this->charWidth;
        col += repeats - 1;
      }

      // skip the rest of the line (is out of view)

      while (this->textBuffer[pos] != '\n' && this->textBuffer[pos] != 0)
      {
        ++pos;
        --cursorOffset;
      }

      if (this->textBuffer[pos] == 0)
        break;

      // move to the next line

      ++pos;
      --cursorOffset;

      drawY += this->charHeight;
    }

    POK::display.color = colorBackup;
    POK::display.bgcolor = backgroundBackup;
    POK::display.setFont(fontBackup);
  }
};

/**
  Displays and handles a keyboard for the user to input some text.

  @param prompText text that will be displayed as a prompt
  @param answerBuffer text buffer to which the answer will be read, must
                      be initialized with starting text
  @param answerBufferSize size of answerBuffer in bytes
  @param x x coordinate to render the keyboard at
  @param y y coordinate to render the keyboard at

  @return true if confirmed, false if cancelled
*/

bool popUpKeyboard(const char *prompText, char *answerBuffer, size_t answerBufferSize,
  int16_t x, int16_t y)
{
  Keyboard keyboard;
  PopUpAnswerKeyboardHandler<char> handler(&keyboard, answerBuffer, answerBufferSize);
  return handler.promptAnswer(prompText,x,y);
}

#undef POK

#endif // guard
