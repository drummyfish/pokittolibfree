#ifndef TEXT_EDITOR_POKITTO_H
#define TEXT_EDITOR_POKITTO_H

/**
  Pokitto text editor base class.
  Author: Miloslav Ciz
  License: CC0
*/

#include "Pokitto.h"
#include "SDFileSystem.h"
#include <cstdint>
#include "keyboard.hpp"
#include "asciiftw.h"

#ifndef TEXT_ED_ABOUT
  #define TEXT_ED_ABOUT "Pokitto text editor."
#endif

Pokitto::Core pokitto;

SDFileSystem *sdFs;

const char emptyStr = 0;

/// Helper to convert numbers to strings.
void numToStr(uint16_t num, char *dst)
{
  for (uint8_t i = 0; i < 5; ++i)
    dst[i] = ' ';

  uint8_t pos = 4;

  while (num != 0)
  {
    dst[pos] = '0' + (num % 10);
    num /= 10;
    --pos;
  }
}

inline bool isBlank(char c)
{
  return c == ' ' || c == '\n' || c == '\t';
}

enum ColorIndex: uint8_t
{
  text,             // 0
  background,       // 1
  buttonBackground, // 2
  buttonText,       // 3
  special,          // 4
  detail            // 5
};

inline char toLower(char c)
{
  if (c >= 'A' && c <= 'Z')
    return c + ('a' - 'A');

  return c;
}

void copyStr(const char *source, char *dst, size_t dstSize, bool terminate = true)
{
  size_t end = dstSize - (terminate ? 1 : 0);
  size_t i;

  for (i = 0; i < end; ++i)
  {
    char c = source[i];
    dst[i] = c;
  }

  if (terminate)
    dst[i] = 0;
}

struct Stats
{
  uint16_t characters;
  uint16_t words;
  uint16_t lines;
  uint16_t maxColumns;
};

struct MenuItem
{
protected:
  const char *text;
public:

  bool hasSeparator;

  MenuItem(const char *text = 0, bool hasSeparator = false)
  {
    this->hasSeparator = hasSeparator;
    this->text = text;
  }

  const char *getText()
  {
    return this->text != 0 ? this->text : &emptyStr;
  }
};

template <int TEMPL_TEXT_BUF_LEN,       // length of the buffer for the document text
          int TEMPL_ANS_BUF_LEN,        // length of the buffer for short text answers
          int TEMPL_CLIP_LEN,           // length of clipboard buffer
          int TEMPL_MAX_FIL_NAM,        // maximum number of files to list
          int TEMPL_MAX_FIL_NAM_LEN,    // maximum file name length
          int TEMPL_DOC_NAME_BUF_LEN,   // document name buffer length
          int TEMPL_MENU_ITEMS>         // number of menu items

class TextEditor
{
protected:
  static const int INFOBAR_HEIGHT = 9;

  static const int TEXT_BUFFER_LEN = TEMPL_TEXT_BUF_LEN;
  char textBuffer[TextEditor::TEXT_BUFFER_LEN];

  static const int ANSWER_BUFFER_LEN = TEMPL_ANS_BUF_LEN;
  char answerBuffer[TextEditor::ANSWER_BUFFER_LEN];

  static const int CLIPBOARD_LEN = TEMPL_CLIP_LEN;
  char clipboard[CLIPBOARD_LEN];

  static const int MAX_FILE_NAMES = TEMPL_MAX_FIL_NAM;
  static const int MAX_FILE_NAME_LENGTH = TEMPL_MAX_FIL_NAM_LEN;
  char fileNames[MAX_FILE_NAMES][MAX_FILE_NAME_LENGTH];

  static const int DOCUMENT_NAME_BUFFER_LEN = TEMPL_DOC_NAME_BUF_LEN;
  char documentNameBuffer[DOCUMENT_NAME_BUFFER_LEN];

  int16_t viewX = 0;
  int16_t viewY = 0;

  int8_t theme = 0;
  int8_t layout = 0;

  bool menuOpen = false;

  size_t selectedMenuItem = 0;
  size_t selectedFileIndex = 0;

  Keyboard keyboard;

  MultiLineKeyboardHandler<char> handler = MultiLineKeyboardHandler<char>(&this->keyboard,textBuffer,TextEditor::TEXT_BUFFER_LEN);
  PopUpAnswerKeyboardHandler<char> answerHandler = PopUpAnswerKeyboardHandler<char>(&this->keyboard,answerBuffer,TextEditor::ANSWER_BUFFER_LEN);

  TextView textView = TextView(textBuffer,font3x5,108,88 - Keyboard::HEIGHT - TextEditor::INFOBAR_HEIGHT - 2);

  static const int16_t MENU_ITEM_COUNT = TEMPL_MENU_ITEMS;

  MenuItem menuItems[MENU_ITEM_COUNT];

  void drawInfobar()
  {
    int16_t y = pokitto.display.height - TextEditor::INFOBAR_HEIGHT + 1;

    pokitto.display.setColor(ColorIndex::buttonBackground);
    pokitto.display.fillRect(0,y,pokitto.display.width,TextEditor::INFOBAR_HEIGHT);
    pokitto.display.setColor(ColorIndex::buttonText);
    pokitto.display.drawFastHLine(0,y,pokitto.display.width);

    y += 2;

    pokitto.display.setColor(ColorIndex::buttonText);
    pokitto.display.bgcolor = ColorIndex::buttonBackground;
    pokitto.display.setCursor(1,y);

    pokitto.display.print('[');
    pokitto.display.print(this->handler.getCursorColumn() + 1);
    pokitto.display.print(';');
    pokitto.display.print(this->handler.getCursorRow() + 1);
    pokitto.display.print(']');

    pokitto.display.setCursor(38,y);
    pokitto.display.print(this->documentNameBuffer);

    if (this->menuOpen)
    {
      pokitto.display.setColor(ColorIndex::special);
      pokitto.display.fillRect(89,y - 1,21,TextEditor::INFOBAR_HEIGHT);
      pokitto.display.bgcolor = ColorIndex::special;
    }

    pokitto.display.setColor(ColorIndex::detail);
    pokitto.display.drawRect(89,y - 1,21,TextEditor::INFOBAR_HEIGHT);

    pokitto.display.setColor(this->menuOpen ? 1 : 3);
    pokitto.display.setCursor(92,y);
    pokitto.display.print("Menu");

    pokitto.display.bgcolor = ColorIndex::background;
  }

  virtual void drawMenu()
  {
    static const int16_t padding = 2;
    static const int16_t spacing = 6;
    static const int16_t height = MENU_ITEM_COUNT * spacing + 2 * padding;
    static const int16_t x = 65;
    static const int16_t y = 88 - height - TextEditor::INFOBAR_HEIGHT;

    pokitto.display.setColor(ColorIndex::detail);
    pokitto.display.fillRect(x,y,pokitto.display.width - x,height);

    pokitto.display.setColor(ColorIndex::text);
    pokitto.display.drawRect(x,y,pokitto.display.width - x,height);

    for (uint8_t i = 0; i < MENU_ITEM_COUNT; ++i)
    {
      pokitto.display.setCursor(x + padding,y + padding + spacing * i);
      pokitto.display.bgcolor = ColorIndex::detail;
      pokitto.display.setColor(ColorIndex::buttonText);

      if (i == this->selectedMenuItem)
      {
        pokitto.display.bgcolor = ColorIndex::special;
        pokitto.display.setColor(ColorIndex::special);
        pokitto.display.fillRect(x + padding - 1,y + padding + spacing * i - 1,pokitto.display.width - x - 2 * padding + 2,spacing);
        pokitto.display.setColor(ColorIndex::background);
      }
      else
        pokitto.display.bgcolor = ColorIndex::detail;

      pokitto.display.print(this->menuItems[i].getText());

      if (this->menuItems[i].hasSeparator)
      {
        pokitto.display.setColor(ColorIndex::buttonBackground);
        pokitto.display.drawFastHLine(x + 1,y + padding + spacing * (i + 1) - 1,pokitto.display.width - x);
      }
    }
  }

  virtual void setKeyboardLayout(uint8_t layout)
  {
    this->layout = layout % 3;
 
    switch (this->layout)
    {
      case 0: this->keyboard.setLayout(Keyboard::layoutABC); break;
      case 1: this->keyboard.setLayout(Keyboard::layoutQWERTY); break;
      case 2: this->keyboard.setLayout(Keyboard::layoutQWERTZ); break;
      default: break;
    }
  }

  virtual Stats computeStats()
  {
    Stats result;

    size_t pos = 0;

    bool waitingForWord = isBlank(this->textBuffer[0]);

    result.characters = 0;
    result.lines = 1;
    result.words = 0;
    result.maxColumns = 0;

    size_t columns = 0;

    while (this->textBuffer[pos] != 0)
    {
      result.characters++;
      columns++;

      if (textBuffer[pos] == '\n')
      {
        if (columns > result.maxColumns)
          result.maxColumns = columns;

        result.lines++;
        columns = 0;
      }

      if (isBlank(this->textBuffer[pos]))
      {
        waitingForWord = true;
      }
      else if (waitingForWord)
      {
        result.words++;
        waitingForWord = false;
      }

      ++pos;
    }

    return result;
  }

  virtual void draw()
  {
    this->keyboard.draw(0,pokitto.display.height - Keyboard::HEIGHT - TextEditor::INFOBAR_HEIGHT);
    this->textView.draw(1,1,this->handler.getCursorPosition(),this->handler.getCursorRow() - this->viewY,this->viewX,this->handler.isSelecting(),this->handler.getSelectionStart());
    drawInfobar();

    if (this->menuOpen)
      drawMenu();
  }

  virtual void setTheme(uint8_t number)
  {
    #define palRGB(n,r,g,b) pokitto.display.palette[n] = pokitto.display.RGBto565(r,g,b)

    switch (number)
    {
      case 0:
        palRGB(ColorIndex::text,0,0,0);      
        palRGB(ColorIndex::background,255,255,255);
        palRGB(ColorIndex::buttonBackground,190,190,190);
        palRGB(ColorIndex::buttonText,90,90,90);   
        palRGB(ColorIndex::special,128,30,64);  
        palRGB(ColorIndex::detail,220,220,220);
        break;

      case 1:
        palRGB(ColorIndex::text,10,34,14);
        palRGB(ColorIndex::background,221,247,247);
        palRGB(ColorIndex::buttonBackground,204,181,204);
        palRGB(ColorIndex::buttonText,76,96,79);
        palRGB(ColorIndex::special,5,91,81);
        palRGB(ColorIndex::detail,191,226,219);
        break;

      case 2:
        palRGB(ColorIndex::text,0,0,0);
        palRGB(ColorIndex::background,255,255,255);
        palRGB(ColorIndex::buttonBackground,255,255,255);
        palRGB(ColorIndex::buttonText,0,0,0);
        palRGB(ColorIndex::special,0,0,0);
        palRGB(ColorIndex::detail,255,255,255);
        break;

      case 3:
        palRGB(ColorIndex::text,255,255,255);
        palRGB(ColorIndex::background,0,0,0);
        palRGB(ColorIndex::buttonBackground,255,255,255);
        palRGB(ColorIndex::buttonText,100,0,0);
        palRGB(ColorIndex::special,0,200,0);
        palRGB(ColorIndex::detail,255,255,255);
        break;
 
      default:
        break;
    }

    this->keyboard.setColors(
      ColorIndex::buttonText,
      ColorIndex::buttonBackground,
      ColorIndex::background,
      ColorIndex::text,
      ColorIndex::special,
      ColorIndex::detail);

    this->textView.setColors(
      ColorIndex::text,
      ColorIndex::background,
      ColorIndex::background,
      ColorIndex::special);
  }

  virtual bool copy()
  {
    if (!this->handler.isSelecting())
      return false;

    size_t selectionFrom = min(this->handler.getSelectionStart(),this->handler.getCursorPosition());
    size_t selectionTo = max(this->handler.getSelectionStart(),this->handler.getCursorPosition());
    size_t clipboardPos = 0;

    for (size_t i = selectionFrom; i < min(selectionTo,TextEditor::CLIPBOARD_LEN - 2); ++i)
    {
      this->clipboard[clipboardPos] = this->textBuffer[i];

      if (clipboardPos >= TextEditor::CLIPBOARD_LEN)
        return false;

      clipboardPos++;
    } 

    this->clipboard[clipboardPos] = 0;

    this->handler.unselect();

    return true;
  }

  virtual bool paste()
  {
    if (this->handler.isSelecting())
      this->handler.deleteSelection();

    size_t clipboardPos = 0;

    while (clipboardPos < TextEditor::CLIPBOARD_LEN && this->clipboard[clipboardPos] != 0)
    {
      this->handler.put(this->clipboard[clipboardPos]);
      clipboardPos++;
    }

    return true;
  }

  virtual void initNewFile()
  {
    this->textBuffer[0] = 0;
    this->setMenuOpen(false);
    this->handler.setCursorPosition(0);

    copyStr("new.txt",this->documentNameBuffer,TextEditor::DOCUMENT_NAME_BUFFER_LEN);
  }

  void copySelectionToAnswer()
  {
    if (!this->handler.isSelecting())
      return;

    size_t selectionFrom = min(this->handler.getSelectionStart(),this->handler.getCursorPosition());
    size_t selectionTo = max(this->handler.getSelectionStart(),this->handler.getCursorPosition());

    size_t pos = 0;
    
    for (size_t i = selectionFrom; i < selectionTo; ++i)
    {
      this->answerBuffer[pos] = this->textBuffer[i] != '\n' ? this->textBuffer[i] : ' ';

      pos++;

      if (pos >= TextEditor::ANSWER_BUFFER_LEN - 1)
        break;
    }

    this->answerBuffer[pos] = 0;
  }

  bool search(const char *what)
  {
    size_t whatLen = 0;

    while (what[whatLen] != 0)
      ++whatLen;

    size_t pos = this->handler.getCursorPosition();

    do
    {
      bool match = true;

      for (size_t i = 0; i < whatLen; ++i)
      {
        size_t index = pos + i;

        if (index >= TextEditor::TEXT_BUFFER_LEN)
        {
          match = false;
          break;
        }

        char c = this->textBuffer[index];

        if (toLower(what[i]) != toLower(c))
        {
          match = false;
          break;
        }
      }

      if (match)
      {
        this->handler.select(pos,pos + whatLen);
        this->adjustView();
        return true;
      }

      ++pos;

      if (pos >= TextEditor::TEXT_BUFFER_LEN)
        pos = 0;
    }
    while (pos != this->handler.getCursorPosition());

    return false;
  }

  void setMenuOpen(bool open)
  {
    this->menuOpen = open;
    this->keyboard.hasFocus = !open;
  }

  virtual void handleMenuItem(uint16_t itemIndex)
  {
     switch (itemIndex)
     {
       case 0: // new
         if (this->popUpWindow("Really new file?",true))
           this->initNewFile();

         break;

       case 1: // open
       {
         char *fileName = this->fileSelectDialog();

         if (fileName != 0 && fileName[0] != 0 && this->popUpWindow("Really open?",true))
           this->openFile(fileName);

         this->setMenuOpen(false);

         break;
       }

       case 2: // save as
       {
         copyStr(this->documentNameBuffer,this->answerBuffer,TextEditor::ANSWER_BUFFER_LEN);

         bool confirmed = this->answerHandler.promptAnswer("save as",0,
           pokitto.display.height - INFOBAR_HEIGHT - Keyboard::HEIGHT - PopUpAnswerKeyboardHandler<char>::EXTRA_HEIGHT);

         if (confirmed)
           this->saveFile(this->answerBuffer);
         
         this->setMenuOpen(false);

         break;
       }

       case 3: // copy
         this->copy();
         this->setMenuOpen(false);
         break;

       case 4: // paste
         this->paste();
         this->setMenuOpen(false);
         this->adjustView();
         break;

       case 5: // search
       {
         if (this->handler.isSelecting())
           this->copySelectionToAnswer();
         else
           this->answerBuffer[0] = 0;

         bool confirmed = this->answerHandler.promptAnswer("search for",0,
           pokitto.display.height - INFOBAR_HEIGHT - Keyboard::HEIGHT - PopUpAnswerKeyboardHandler<char>::EXTRA_HEIGHT);

         if (confirmed)
         {
           if (!this->search(this->answerBuffer))
             this->popUpWindow("Nothing found.");
         }

         break;
       }

       case 6: // theme
         this->theme = (this->theme + 1) % 4; this->setTheme(this->theme);
         break;

       case 7: // layout
         this->setKeyboardLayout(this->layout + 1);
         break;

       case 8: // stats
       {
         Stats stats = this->computeStats();

         char statStr[] = "chars: xxxxx\nlines: xxxxx\nwords: xxxxx\ncolumns: xxxxx";

         numToStr(stats.characters,statStr + 7);
         numToStr(stats.lines,statStr + 20);
         numToStr(stats.words,statStr + 33);
         numToStr(stats.maxColumns,statStr + 48);

         this->popUpWindow(statStr);
         break;
       }

       case 9: // about
         this->popUpWindow(TEXT_ED_ABOUT);
         break;

       default:
         break;
     }
  }

  virtual void handleInput()
  {
    if (!this->menuOpen)
    {
      if (this->keyboard.selectionY == Keyboard::KEYS_Y - 1 &&
          this->keyboard.selectionX >= Keyboard::KEYS_X - 3 &&
          pokitto.buttons.timeHeld(BTN_DOWN) > 2 &&
          pokitto.buttons.timeHeld(BTN_B) < 1 &&
          pokitto.buttons.timeHeld(BTN_C) < 1)
      {
        this->setMenuOpen(true);
      }
    }
    else
    {
      if (pokitto.buttons.pressed(BTN_B))
      {
        this->setMenuOpen(false);
      }
      else if (pokitto.buttons.repeat(BTN_DOWN,4))
        this->selectedMenuItem = this->selectedMenuItem < MENU_ITEM_COUNT - 1 ? this->selectedMenuItem + 1 : 0;
      else if (pokitto.buttons.repeat(BTN_UP,4))
        this->selectedMenuItem = this->selectedMenuItem > 0 ? this->selectedMenuItem - 1 : MENU_ITEM_COUNT - 1;
      else if (pokitto.buttons.pressed(BTN_A))
        this->handleMenuItem(this->selectedMenuItem);
    }

    this->adjustView();
  }

  void drawSmallButton(int16_t x, int16_t y, char *text, bool highlighted)
  {
    pokitto.display.setColor(highlighted ? ColorIndex::special : ColorIndex::background);
    pokitto.display.bgcolor = pokitto.display.color;
    pokitto.display.fillRect(x - 10,y - 5,20,10);

    if (highlighted && (pokitto.frameCount >> 2) % 2 == 0)
    {
      pokitto.display.setColor(ColorIndex::text);
      pokitto.display.drawRect(x - 11,y - 6,21,12);
    }

    pokitto.display.setColor(highlighted ? ColorIndex::background : ColorIndex::buttonText);
    pokitto.display.setCursor(x - 7,y - 2);
    pokitto.display.print(text);
  }

  virtual bool popUpWindow(const char *text, bool confirmDialog = false)
  {
    static const int16_t padding = 12;

    pokitto.display.persistence = true;

    bool confirmed = false;

    int16_t x, y, w, h, x2, y2;

    x = padding;
    y = padding / 2;
    w = pokitto.display.width - padding * 2;
    h = pokitto.display.height - padding * 2;

    x2 = x + w / 2;
    y2 = y + h - 13;

    int itemSelected = 1;

    while (pokitto.isRunning())
    {
      if (pokitto.update())
      {
        pokitto.display.bgcolor = ColorIndex::buttonBackground;
        pokitto.display.setColor(ColorIndex::buttonBackground);
        pokitto.display.fillRect(x,y,w,h);
        pokitto.display.setColor(ColorIndex::text);
        pokitto.display.drawRect(x,y,w,h);
        pokitto.display.setColor(ColorIndex::detail);
        pokitto.display.drawRect(x + 1, y + 1, w - 2, h - 2);

        pokitto.display.setColor(ColorIndex::buttonText);

        int8_t line = 0;
        int8_t col = 0;
        size_t pos = 0;

        while (line < 5)
        {
          pokitto.display.setCursor(padding + 6, padding / 2 + 6 + line * 6);

          while (text[pos] != '\n' && text[pos] != 0 && col < 19)
          {
            pokitto.display.print(text[pos]);
            ++pos;
            ++col;
          }

          if (text[pos] == 0)
            break;
          else if (text[pos] != '\n')
            pokitto.display.print(text[pos]);
            
          ++pos;
          ++line;
          col = 0;
        }

        if (confirmDialog)
        {
          this->drawSmallButton(x2 - 12,y2,(char *) "yes",itemSelected == 0);
          this->drawSmallButton(x2 + 12,y2,(char *) "no",itemSelected == 1);
        }
        else
        {
          this->drawSmallButton(x2,y2,(char *) "OK",true);
        }

        if (pokitto.buttons.pressed(BTN_LEFT) && itemSelected == 1)
          itemSelected = 0;
        else if (pokitto.buttons.pressed(BTN_RIGHT) && itemSelected == 0)
          itemSelected = 1;
        else if (pokitto.buttons.pressed(BTN_B))
          break;
        else if (pokitto.buttons.pressed(BTN_A))
        {
          confirmed = itemSelected == 0;
          break;
        }
      }
    }

    pokitto.display.persistence = false;

    return confirmed;
  }

  virtual void adjustView()
  {
    size_t columnWithFullTabs = this->handler.getCursorColumn() + this->handler.getTabsBeforeCursor() * (this->textView.tabWidth - 1);

    // finding exact column isn't that easy because tab width depends on its position, so let's just approximate
    uint16_t approximateColumn = (columnWithFullTabs + this->handler.getCursorColumn()) / 2;

    if (approximateColumn < this->viewX)
      this->viewX = approximateColumn;
    else if (approximateColumn >= this->viewX + this->textView.getWidthChars())
      this->viewX = approximateColumn - this->textView.getWidthChars() + 1;

    if (((uint16_t) this->handler.getCursorRow()) < this->viewY)
      this->viewY = this->handler.getCursorRow();
    else if (((uint16_t) this->handler.getCursorRow()) >= this->viewY + this->textView.getHeightChars())
      this->viewY = this->handler.getCursorRow() - this->textView.getHeightChars() + 1;
  }

  char *fileSelectDialog()
  {
    // load the file list

    char *fileName;
    size_t i = 0;

    DirHandle *dir = sdFs->opendir("/");

    while (true)
    {
      fileName = dir->readdir()->d_name;

      if (i >= TextEditor::MAX_FILE_NAMES - 1 || fileName == 0 || fileName == NULL || fileName[0] == 0)
        break;

      copyStr(fileName,this->fileNames[i],MAX_FILE_NAME_LENGTH);
      ++i; 
    }

    dir->closedir();

    this->fileNames[i][0] = 0;

    bool wait = true; // makes us wait one extra frame for A release

    // open the dialog

    while (pokitto.isRunning())
    {
      if (pokitto.update())
      {        
        pokitto.display.bgcolor = ColorIndex::background;
        pokitto.display.clear();

        pokitto.display.setColor(ColorIndex::buttonBackground);
        pokitto.display.fillRect(0,0,110,9);

        pokitto.display.setColor(ColorIndex::buttonText);
        pokitto.display.setCursor(1,1);

        pokitto.display.bgcolor = ColorIndex::buttonBackground;
        pokitto.display.print("select a file");
        pokitto.display.bgcolor = ColorIndex::background;
        
        size_t i = 0;
        size_t numFiles = TextEditor::MAX_FILE_NAMES;

        for (i = 0; i < TextEditor::MAX_FILE_NAMES; ++i)
          if (this->fileNames[i][0] == 0)
          {
            numFiles = i;
            break;
          }
    
        int16_t displayFiles = min(8,numFiles);

        i = min(
              max(0,((int16_t) this->selectedFileIndex) - displayFiles / 2 + 1),
              ((int16_t) numFiles) - displayFiles);

        size_t endIndex = i + displayFiles;

        int16_t drawY = 16;

        while (i < endIndex)
        {
          if (i != selectedFileIndex)
          {
            pokitto.display.setColor(ColorIndex::text);
            pokitto.display.bgcolor = ColorIndex::background;
          }
          else
          {
            pokitto.display.bgcolor = ColorIndex::special;
            pokitto.display.setColor(ColorIndex::special);
            pokitto.display.fillRect(1,drawY - 1,108,6);
            pokitto.display.setColor(ColorIndex::background);
          }

          pokitto.display.setCursor(2,drawY);
          pokitto.display.print(this->fileNames[i]);

          ++i;
          drawY += 8;
        }

        if (pokitto.buttons.repeat(BTN_UP,4))
          this->selectedFileIndex -= this->selectedFileIndex > 0 ? 1 : 0;
        else if (pokitto.buttons.repeat(BTN_DOWN,4))
          this->selectedFileIndex += this->selectedFileIndex < numFiles - 1 ? 1 : 0;
      }

      if (pokitto.buttons.pressed(BTN_B))
        return 0;
      
      if (wait)
        wait = false;
      else if (pokitto.buttons.pressed(BTN_A))
        return this->fileNames[this->selectedFileIndex];
    }

    return 0;
  }

  virtual void openFile(char *fileName)
  {
    FileHandle *file = sdFs->open(fileName,O_RDONLY);

    if (file == NULL)
    {
      this->popUpWindow("Could not open\nthe file!");
      return;
    }

    off_t fileLen = file->flen();

    ssize_t numBytes = file->read(this->textBuffer,TextEditor::TEXT_BUFFER_LEN);

    if (numBytes < 0)
    {
      this->popUpWindow("Error opening\nthe file!");
      return;
    }
    else if (numBytes != fileLen)
    {
      this->popUpWindow("The document was\ncut because it\nwas too long.");
    }

    file->close();

    this->textBuffer[min(numBytes,TextEditor::TEXT_BUFFER_LEN - 1)] = 0;
    asciifyThatBoy(this->textBuffer); // convert the loaded text into something sane

    this->handler.setCursorPosition(0);

    copyStr(fileName,this->documentNameBuffer,TextEditor::DOCUMENT_NAME_BUFFER_LEN);

    this->adjustView();
  }

  bool fileExists(char *fileName)
  {
    FileHandle *file = sdFs->open(fileName,O_RDONLY);
   
    if (file != 0)
    {
      file->close();
      return true;
    } 
    
    return false;
  }

  bool dirExists(char *dirName)
  {
    DirHandle *dir = sdFs->opendir(dirName);

    if (dir != 0)
    {
      dir->closedir();
      return true;
    }
 
    return false;
  }

  virtual void saveFile(char *fileName)
  {
    if (this->dirExists(fileName))
    {
      this->popUpWindow("Such directory\nexists - can\nnot save!");
      return;
    }

    if (!this->fileExists(fileName))
    {
      FileHandle *file = sdFs->open(fileName,O_CREAT);

      if (file == NULL)
      {
        this->popUpWindow("Could not\ncreate new\nfile!");
        return;
      }

      file->close();
    }

    FileHandle *file = sdFs->open(fileName,O_WRONLY);

    if (file == NULL)
    {
      this->popUpWindow("Could not save\nthe file!");
      return;
    }

    size_t size = 0;

    while (this->textBuffer[size] != 0)
      ++size;

    ssize_t numBytes = file->write(this->textBuffer,size);

    file->close();

    if (numBytes < 0)
      this->popUpWindow("Error writing\nto file!");
    else
      this->popUpWindow("Saved successfully.");
  }

  virtual void initMenuItems()
  {
    size_t i = 0;

    #define nextItem i++; if (i >= TextEditor::MENU_ITEM_COUNT) return;

    this->menuItems[i] = MenuItem("new");
    nextItem
    this->menuItems[i] = MenuItem("open");
    nextItem
    this->menuItems[i] = MenuItem("save as",true);
    nextItem
    this->menuItems[i] = MenuItem("copy");
    nextItem
    this->menuItems[i] = MenuItem("paste");
    nextItem
    this->menuItems[i] = MenuItem("search",true);
    nextItem
    this->menuItems[i] = MenuItem("theme");
    nextItem
    this->menuItems[i] = MenuItem("layout",true);
    nextItem
    this->menuItems[i] = MenuItem("stats");
    nextItem
    this->menuItems[i] = MenuItem("about");
    nextItem

    #undef nextItem
  }

  virtual void init()
  {
    pokitto.setFrameRate(60);

    pokitto.display.setFont(font3x5);

    this->initMenuItems();
    this->setKeyboardLayout(1);

    this->setTheme(0);

    size_t pos = 0;

    clipboard[0] = 0;

    while (pos < TextEditor::TEXT_BUFFER_LEN)
    {
      this->textBuffer[pos] = 0;
      pos++;
    }

    for (size_t i = 0; i < MAX_FILE_NAMES; ++i)
      this->fileNames[i][0] = 0;

    this->initNewFile();
  }

public:
  TextEditor()
  {
  }

  virtual void run()
  {
    pokitto.begin();
    SDFileSystem sd(P0_9,P0_8,P0_6,P0_7,"sd");

    sdFs = &sd;
    this->init();

    while (pokitto.isRunning())
    {
      if (pokitto.update())
      {
        draw();
        this->handler.update();
        handleInput();

        pokitto.display.bgcolor = ColorIndex::background;
      }
    }
  }
};

#endif
