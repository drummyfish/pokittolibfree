# Text Editing and Scripting on Pokitto

This repo contains multiple programs for an open educational console Pokitto.
The programs include:

- reusable onscreen keyboard and text editing library, **keyboard.hpp**
- extendable plaintext editor, **PokiPad**, using keyboard.hpp
- simple reusable **LIL interpreter** (LIL is a small interpreted language)
- **LILIDE**, a text "IDE" based on PokiPad and LIL
- small reusable "fun" library **asciiftw.h** that serves to convert text with general encoding to plain ASCII

![](preview.gif)

Everything should be more or less documented in the code. The .h modules
(keyboard.hpp, pokittolil.hpp, asciiftw.h) are independent and usable by
themselves, so you can simply include them in your project and use them. The
two text editors are based on a common class in texteditor.hpp, which you can
reuse as well if you want to make your own fine-tuned editor for Pokitto.

## License

Everything that was made by me (drummyfish) is released under CC0 1.0,
public domain. The LIL source code (lil.c and lil.h) is released under its own
permissive free license that can be found within the files.