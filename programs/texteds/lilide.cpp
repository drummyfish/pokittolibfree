/**
  LILIDE, simple LIL language IDE for Pokitto.

  author: Miloslav Ciz AKA drummyfish

  author of the LIL source code (lil.c and lil.h) is Kostas Michalopoulos.
  These files are released under its own free license, found within the
  files.

  This file (lilide.cpp) and other ones written solely by drummyfish are
  released under CC0 1.0.
*/

#define TEXT_ED_ABOUT "LILIDE\nv1.1\nunder CC0\nby drummyfish"
#include "texteditor.hpp"
#include "pokittolil.hpp"

const char example1[] =
"## bouncing ball ##\n\n"
"set x 50\n"
"set y 40\n"
"set d 8\n"
"set h 1 ## hor speed ##\n"
"set v 1 ## vert speed ##\n\n"
"while {[p_pressed 1] != 1} {\n"
"  inc x $h \n"
"  inc y $v \n"
"  if {$x > 110 - $d} {set h -1} { if {$x < $d} {set h 1}}\n"
"  if {$y > 88 - $d} {set v -1} { if {$y < $d} {set v 1}}\n"
"  p_clear\n"
"  p_circle $x $y $d 6 1\n"
"  p_update\n"
"}\n";

const char example2[] =
"func bool2str {b} { if {$b} {return yes} {return no} }\n\n"

"while {[p_pressed 2] != 1} {\n"
"  p_clear 7\n"
"  p_write [repstr \"A pressed: x\" x [bool2str [p_pressed 0]]] 1 1\n"
"  p_write [repstr \"B pressed: x\" x [bool2str [p_pressed 1]]] 1 10\n"
"  p_write [repstr \"time: x\" x [p_time]] 1 20\n"
"  p_write [repstr \"frame: x\" x [p_frame]] 1 30\n"
"  p_write \"press C to exit\" 1 40 5\n"
"  p_update\n"
"}\n";

const char example3[] =
"## random art ##\n\n"
"p_write \"computing art...\"\n"
"p_update\n\n"
"p_clear [p_rand]\n"
"set n [expr 20 + [p_rand]]\n\n"
"for {set i 0} {$i < $n} {inc i} {\n"
"  set x [expr [p_rand] - 50] \n"
"  set y [expr [p_rand] - 50] \n"
"  set shape [expr [p_rand] % 5]\n\n"
"  if {shape == 0} { p_line $x $y [p_rand] [p_rand] [p_rand] }\n"
"  if {shape == 1} { p_rect $x $y [expr [p_rand] % 20] [expr [p_rand] % 20] [p_rand] [expr [p_rand] % 2] }\n"
"  if {shape == 2} { p_circle $x $y [expr [p_rand] % 20] [p_rand] [expr [p_rand] % 2] }\n"
"  if {shape == 3} { p_pixel $y $x [p_rand] }\n"
"}\n\n"
"p_read ## wait for button ##";

const char example4[] =
"## sin ##\n\n"
"while {[p_pressed 2] != 1} {\n"
"  p_clear\n"
"  set x [p_sin [p_frame]]\n"
"  set y [p_sin [expr [p_frame] + 25]]\n"
"  set d [expr 8 + ($y + 100) \\ 16]\n"
"  set x [expr $x \\ 2]\n"
"  set y [expr $y \\ 4]\n"
"  p_circle [expr 55 + $x] [expr 44 + $y] $d 7\n"
"  p_circle [expr 55 - $x] [expr 44 - $y] [expr 20 - $d + 8)] 6\n"
"  p_update\n"
"}\n";

const char example5[] =
"## movement ##\n\n"
"set x 20\n"
"set y 30\n\n"
"while {[p_pressed 1] != 1} {\n"
"  p_clear 1\n"
"  if {[p_pressed 3]} {inc y -1}\n"
"  if {[p_pressed 4]} {inc x }\n"
"  if {[p_pressed 5]} {inc y }\n"
"  if {[p_pressed 6]} {inc x -1}\n"
"  p_write \"use arrows to move\" 10 10 2\n"
"  p_rect $x $y 10 10 0\n"
"  p_update\n"
"}\n";

const char example6[] =
"## pendulum and floats ##\n\n"
"set a -20.0 ## angle ##\n"
"set v 0.0 ## velocity ##\n\n"
"while {[p_pressed 1] != 1} {\n"
"  p_clear\n"
"  set x [expr 55 + [p_sin $a] \\ 2]\n"
"  set y [expr 2 + [p_sin [expr $a + 25]] \\2]\n"
"  p_line 55 2 $x $y 3\n"
"  p_circle $x $y 5 4 1\n"
"  if {$a > 0} {inc v -0.1} {inc v 0.1}\n"
"  inc a $v\n"
"  p_write [repstr \"angle: x\" x $a] 1 70 1\n"
"  p_write [repstr \"velocity: x\" x $v] 1 80 1\n"
"  p_update\n"
"}\n";

#define NUM_EXAMPLES 6

const char* examples[NUM_EXAMPLES] =
  {example1, example2, example3, example4, example5, example6};

const char startProgram[] = "p_write hello\np_read";

#define LILIDE_MENU_ITEMS 12

#define ALL_TEMPLATES TEMPL_TEXT_BUF_LEN,TEMPL_ANS_BUF_LEN,TEMPL_CLIP_LEN,TEMPL_MAX_FIL_NAM,TEMPL_MAX_FIL_NAM_LEN,TEMPL_DOC_NAME_BUF_LEN,LILIDE_MENU_ITEMS

template <int TEMPL_TEXT_BUF_LEN,  
          int TEMPL_ANS_BUF_LEN,      
          int TEMPL_CLIP_LEN,         
          int TEMPL_MAX_FIL_NAM,      
          int TEMPL_MAX_FIL_NAM_LEN,  
          int TEMPL_DOC_NAME_BUF_LEN>       

class LILIde: public TextEditor<ALL_TEMPLATES>
{
protected:
  LILInterpreter interpreter;
  uint8_t nextExample = 0;

  void runLil()
  {
    LILInterpreter::RunResult result = this->interpreter.runCode(this->textBuffer);

    if (!result.ok)
    {
      Pokitto::Core::display.bgcolor = 0;
      Pokitto::Core::display.clear();

      this->popUpWindow(result.message);

      // unfortunatelly LIL doesn't seem to have a nice way to get a global error position
      /* this->handler.setCursorPosition(result.position);
         this->adjustView(); */
    }
  }

  virtual void handleMenuItem(uint16_t itemIndex) override
  {
    TextEditor<ALL_TEMPLATES>::handleMenuItem(itemIndex);

    if (itemIndex == LILIDE_MENU_ITEMS - 1)
      this->runLil();
    else if (itemIndex == LILIDE_MENU_ITEMS - 2)
    {
      if (this->popUpWindow("Really load\nan example?",true))
      {
        copyStr(examples[this->nextExample],this->textBuffer,TextEditor<ALL_TEMPLATES>::TEXT_BUFFER_LEN);
        this->nextExample = (this->nextExample + 1) % NUM_EXAMPLES;
        this->handler.setCursorPosition(0);
      }

      this->setMenuOpen(false);
    }
  }

  virtual void initMenuItems() override
  {
    TextEditor<ALL_TEMPLATES>::initMenuItems();
    this->menuItems[LILIDE_MENU_ITEMS - 1] = MenuItem("run LIL");
    this->menuItems[LILIDE_MENU_ITEMS - 2] = MenuItem("example");
    this->menuItems[LILIDE_MENU_ITEMS - 3].hasSeparator = true;
  }

  virtual void initNewFile() override
  {
    TextEditor<ALL_TEMPLATES>::initNewFile();
    copyStr("new.lil",this->documentNameBuffer,TextEditor<ALL_TEMPLATES>::DOCUMENT_NAME_BUFFER_LEN);
    copyStr(startProgram,this->textBuffer,TextEditor<ALL_TEMPLATES>::TEXT_BUFFER_LEN);
  }
};

int main()
{
  LILIde<1024,32,128,32,16,32> ide;
  ide.run();
  return 0;

}
