/**
  PokPad, a text editor for Pokitto.
  Author: Miloslav Ciz
  License: CC0
*/

#define TEXT_ED_ABOUT "PokiPad\nv1.0\nunder CC0\nby drummyfish"
#include "texteditor.hpp"

/**
  Pokitto plain text editor.

  author: Miloslav Ciz
  license: CC0
*/

int main()
{
  TextEditor<10000,32,128,32,32,16,10>  textEditor;
  textEditor.run();
  return 0;
}
