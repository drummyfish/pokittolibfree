/**
  Library that makes your C string ASCII.

  author: Miloslav Ciz
  license: CC0
*/

#ifndef ASCIIFTW
#define ASCIIFTW

const unsigned char ISO885915APPROX[] = // starting from code 160
  " icLeYSSsca<--R-o+23ZuP.z1o>CaY?AAAAAAACEEEEIIIIDNOOOOOx0UUUUYbBaaaaaaaceeeeiiiionooooo/0uuuuyby";

const unsigned char UTF8APPROX[] = // table of some UTF8 approximations
{
  // up to 4 byte code  approx ASCII char
  195, 161,  0,  0,     'a',
  195, 169,  0,  0,     'e',
  195, 173,  0,  0,     'i',
  195, 179,  0,  0,     'o',
  195, 186,  0,  0,     'u',
  197, 175,  0,  0,     'u',
  195, 129,  0,  0,     'A',
  196, 154,  0,  0,     'E',
  199, 143,  0,  0,     'I',
  199, 145,  0,  0,     'O',
  199, 147,  0,  0,     'U',
  197, 174,  0,  0,     'U',
  197, 190,  0,  0,     'z',
  197, 161,  0,  0,     's',
  196, 141,  0,  0,     'c',
  197, 153,  0,  0,     'r',
  196, 143,  0,  0,     'd',
  197, 165,  0,  0,     't',
  197, 136,  0,  0,     'n',
  197, 189,  0,  0,     'Z',
  197, 160,  0,  0,     'S',
  196, 140,  0,  0,     'C',
  197, 152,  0,  0,     'R',
  196, 142,  0,  0,     'D',
  197, 164,  0,  0,     'T',
  197, 135,  0,  0,     'N',
  195, 189,  0,  0,     'y',
  195, 157,  0,  0,     'Y',
  196, 155,  0,  0,     'e',
  196, 154,  0,  0,     'E',
  194, 161,  0,  0,     '!',
  194, 162,  0,  0,     'c',
  194, 163,  0,  0,     'L',
  194, 164,  0,  0,     'c',
  194, 165,  0,  0,     'Y',
  194, 166,  0,  0,     'I',
  194, 167,  0,  0,     'S',
  194, 168,  0,  0,     '\'',
  194, 169,  0,  0,     'c',
  194, 170,  0,  0,     'a',
  194, 171,  0,  0,     '<',
  194, 172,  0,  0,     '-',
  194, 174,  0,  0,     'R',
  194, 175,  0,  0,     '-',
  194, 176,  0,  0,     'o',
  194, 177,  0,  0,     '+',
  194, 178,  0,  0,     '2',
  194, 179,  0,  0,     '3',
  194, 180,  0,  0,     '\'',
  194, 181,  0,  0,     'u',
  194, 182,  0,  0,     'P',
  194, 183,  0,  0,     '.',
  194, 184,  0,  0,     ',',
  194, 185,  0,  0,     '1',
  194, 186,  0,  0,     '0',
  194, 187,  0,  0,     '>',
  194, 188,  0,  0,     '4',
  194, 189,  0,  0,     '2',
  194, 190,  0,  0,     '4',
  194, 191,  0,  0,     '?',
  195, 151,  0,  0,     'x',
  195, 183,  0,  0,     '/',
  195, 184,  0,  0,     '0',
  195, 152,  0,  0,     '0',
  195, 128,  0,  0,     'A',
  195, 129,  0,  0,     'A',
  195, 130,  0,  0,     'A',
  195, 131,  0,  0,     'A',
  195, 159,  0,  0,     'B',
  195, 182,  0,  0,     'o',
  206, 145,  0,  0,     'A',
  206, 146,  0,  0,     'B',
  206, 147,  0,  0,     'L',
  206, 148,  0,  0,     'D',
  206, 149,  0,  0,     'E',
  206, 150,  0,  0,     'Z',
  206, 151,  0,  0,     'H',
  206, 152,  0,  0,     'O',
  206, 153,  0,  0,     'I',
  206, 154,  0,  0,     'K',
  206, 155,  0,  0,     'A',
  206, 156,  0,  0,     'M',
  206, 157,  0,  0,     'N',
  206, 158,  0,  0,     'E',
  206, 159,  0,  0,     'O',
  206, 160,  0,  0,     'N',
  206, 161,  0,  0,     'P',
  206, 163,  0,  0,     'E',
  206, 164,  0,  0,     'T',
  206, 165,  0,  0,     'Y',
  206, 166,  0,  0,     'O',
  206, 167,  0,  0,     'X',
  206, 168,  0,  0,     'Y',
  206, 169,  0,  0,     'O',
  206, 177,  0,  0,     'a',
  206, 178,  0,  0,     'B',
  206, 179,  0,  0,     'y',
  206, 180,  0,  0,     'd',
  206, 181,  0,  0,     'E',
  206, 182,  0,  0,     'Z',
  206, 183,  0,  0,     'n',
  206, 184,  0,  0,     'O',
  206, 185,  0,  0,     'l',
  206, 186,  0,  0,     'K',
  206, 187,  0,  0,     'l',
  206, 188,  0,  0,     'u',
  206, 189,  0,  0,     'v',
  206, 190,  0,  0,     'E',
  206, 191,  0,  0,     'o',
  207, 128,  0,  0,     'n',
  207, 129,  0,  0,     'p',
  207, 130,  0,  0,     'c',
  207, 131,  0,  0,     'o',
  207, 132,  0,  0,     't',
  207, 133,  0,  0,     'v',
  207, 134,  0,  0,     'q',
  207, 135,  0,  0,     'X',
  207, 136,  0,  0,     'Y',
  207, 137,  0,  0,     'w',
  0 // terminator
};

int asciifyThatBoy(char *boy)
{
  size_t pos = 0;
  int changes = 0;

  while (boy[pos] != 0)
  {
    unsigned char c = boy[pos];

    if ((c < 037 && c != '\n' && c != '\t') || c == 127)
    {
      // special chars => ' '
      boy[pos] = ' ';
      changes++;
    }
    else if (c > 127)
    {
      // UTF-8?
      int bytes = 0;
      unsigned char byteSequence[4];

      for (int i = 0; i < 4; ++i)
        byteSequence[i] = 0;

      int looksLikeUTF8 = 1;

      if ((c >> 5) == 6)
        bytes = 2;
      else if ((c >> 4) == 14)
        bytes = 3;
      else if ((c >> 3) == 30)
        bytes = 4;

      if (bytes == 0)
      {
        looksLikeUTF8 = 0;
      }
      else
      {
        byteSequence[0] = c;

        // further check

        for (int i = 1; i < bytes; ++i)
        {
          unsigned char c2 = boy[pos + i];

          if (c2 == 0 || ((c2 >> 6) != 2))
          {
            looksLikeUTF8 = 0;
            break;
          }
          else
            byteSequence[i] = c2;
        }
      }

      if (looksLikeUTF8)
      {
        size_t pos2 = pos + 1;

        do
        {
          // shift text to the left
          boy[pos2] = boy[pos2 + bytes - 1];
          ++pos2;
        } while (boy[pos2] != 0);

        unsigned char newChar = '?';

        pos2 = 0;

        while (UTF8APPROX[pos2] != 0)
        {
          int matches = 1;

          for (int i = 0; i < bytes; ++i)
            if (byteSequence[i] != UTF8APPROX[pos2 + i])
            {
              matches = 0;
              break;
            }

          if (matches)
          {
            newChar = UTF8APPROX[pos2 + 4];
            break;
          }

          pos2 += 5;
        }

        boy[pos] = newChar;
      }
      else
      {
        // no UTF-8, let's assume ISO 8859-15
        boy[pos] = c >= 160 ? ISO885915APPROX[c - 160] : '?';
      }

      changes++;
    }

    ++pos;
  }

  return changes;
}

#endif
