# Pokitto Ray Casting Demos

These are demos showing advanced ray casting (pseudo) 3D rendering technique
using raycastlib (a Pokitto-independent C library I made along with this
project) on a small open gaming console Pokitto.

![](https://gitlab.com/drummyfish/raycastlib/raw/master/media/pokitto1.gif)
![](https://gitlab.com/drummyfish/raycastlib/raw/master/media/pokitto2.gif)
![](https://gitlab.com/drummyfish/raycastlib/raw/master/media/pokitto3.gif)


There are these demos:

- **helloRay**: Hello world program showing the very basic usage of raycastlib
  on Pokitto. If you want to learn about the code, start here.
- **demo1**: Showoff RPG-like program demonstrating most raycastlib features. Controls:
  - arrows: move, rotate
  - A + arrows: strafe, look up/down
  - B: jump
  - C: toggle running
- **demo2**: Classic "flat/1D" ray casting program in a sci-fi mood. Controls:
  - arrows: move, rotate
  - A: strafe
  - B: shoot plasma
- **demo3**: Minecraft-like demo with procedurally generated editable world. Controls:
  - arrows: move, rotate
  - A + arrows: strafe, look up/down
  - B: jump
  - C: toggle square editing (up/down: change height, left/right: change color)

Included are also other tools I created, such as Python scripts for image
conversion to the format I use. Everything should be sufficiently commented.

The last three demos use common definitions from the *general.hpp* file. This
file contains some useful resources that you can use in your own game. You can
of course as well create a game by expanding any of the demos.

There are a lot of compile-time options you can try out (especially in demo2),
like turning various effects on/off, setting the rendering quality etc. Check
out the defines at the beginning of each source code file.

The biggest impact on performance comes from per-pixel operations, so try to
keep the `pixelFunc` as fast as possible. Turn unneeded raycastlib features off
with given defines.

The most effective way of increasing FPS is subsampling -- that means decreasing
horizontal resolution -- it looks uglier, but still very okay as the vertical
resolution still stays the same. You can do this by defining `SUBSAMPLE` to a
specific value (1 = full resolution, 2 = half resolution etc.) **before**
including *general.hpp* header. You can also cut the resolution by e.g. adding
an info bar to the screen (it's better to add a vertical bar to the screen side
as that will also reduce the number of rays cast).

This repository contains the version of raycastlib that I tested the demos
with, but you can find the most recent version of the library
[among my other repositories](https://gitlab.com/drummyfish/raycastlib).

The demos use display Mode13, i.e. 110x88 with 256 colors. I recommend you use
this mode for your games, but the other ones are an option too. I created a
custom general 256-color HSV-based palette that allows for fast brigtness
manipulation of colors, which is important for fog/dimming. Reusing this
palette (or modifying it) will most likely be the best option for your project.

I used GNU Arm Embedded toolchain to compile the programs. Other compilers are
not tested. Don't forget to compile with `-O3`!

# Ideas For Games

Besides the obvious use the demos show, you can try to be more creative and try
e.g.:

- a racing game
- 3D versions of classic 2D array games (snake, tetris, tower defense, ...)
- Casting the rays vertically instead of horizontally (just switch x and y when
  drawing the pixels). Walls become floors and vice versa. Could result in
  interesting games (and maybe faster rendering).

# License

Everything is under CC0 1.0, including all the assets (they are mostly based on
CC0 images I found at OpenGameArt). I always appreciate credit, but it's not
required. More than credit I appreciate if you share your own software as free
and open-source. With that said, use this in any way you want.
