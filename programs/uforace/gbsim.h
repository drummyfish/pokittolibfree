#ifndef GBSIM_H
#define GBSIM_H

#include "Pokitto.h"
#include "PokittoLib/POKITTO_CORE/PokittoCookie.h"
#include "config-gamebuino.h"

Pokitto::Core pokitto;
//Pokitto::Sound sound;

typedef uint8_t byte;
typedef uint16_t Color;

#define WHITE 1
#define GRAY  9
#define DARKGRAY 8
#define BLACK 0 
#define PURPLE 10
#define PINK 2
#define RED 7
#define ORANGE 15
#define BROWN 12
#define BEIGE 11
#define YELLOW 4
#define LIGHTGREEN 5
#define GREEN 13
#define DARKBLUE 14
#define BLUE 6
#define LIGHTBLUE 3

#define BUTTON_A BTN_A
#define BUTTON_B BTN_B
#define BUTTON_C BTN_B
#define BUTTON_MENU BTN_C
#define BUTTON_UP BTN_UP
#define BUTTON_RIGHT BTN_RIGHT
#define BUTTON_DOWN BTN_DOWN
#define BUTTON_LEFT BTN_LEFT

#define map(v,a0,a1,b0,b1) ((b0) + (((v) - (a0)) * ((b1) - (b0)))/ ((a1) - (a0)))

int16_t gb_display_cursorx = 0;
int16_t gb_display_cursory = 0;
bool    gb_display_textwrap = false;
int16_t gb_display_fontheight = 6;
int16_t gb_display_fontwidth = 4;

bool metaModeActive = false;
int16_t metaLoader = 0;
const int16_t metaLoaderMax = 40;

#define SAVE_SIZE ((4 + NAMELENGTH + 1) * NUM_HIGHSCORE)

const uint16_t blockOffsets[] = // save blocks, hard-coded offsets
{
  0,
  4,
  4 + NAMELENGTH + 1,
  4 + NAMELENGTH + 1 + 4,
  4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1,
  4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1 + 4,
  4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1,
  4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1 + 4,
  4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1,
  4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1 + 4 + NAMELENGTH + 1 + 4
};

class MetaSave: public Pokitto::Cookie
{
public:
  uint8_t blocks[SAVE_SIZE];

  MetaSave()
  {
    init();
  }

  void init()
  {
    for (uint16_t i = 0; i < NUM_HIGHSCORE; ++i)
    {
      *((int32_t *) (&blocks[blockOffsets[i * 2]])) = 9999;
    }
  }
};

MetaSave metaSave;

int32_t gb_save_get(uint16_t block)
{
  return
    (0x0000 | (metaSave.blocks[blockOffsets[block] + 0] <<  0)) |
    (0x0000 | (metaSave.blocks[blockOffsets[block] + 1] <<  8)) |
    (0x0000 | (metaSave.blocks[blockOffsets[block] + 2] << 16)) |
    (0x0000 | (metaSave.blocks[blockOffsets[block] + 3] << 24));
}

void gb_save_get_buf(uint16_t block, char *buffer = 0, int size = 0)
{
  for (int i = 0; i < size; ++i)
    buffer[i] = metaSave.blocks[blockOffsets[block] + i];
}

void gb_save_set(uint16_t block, char *buffer, uint8_t buffersize)
{
  for (int i = 0; i < buffersize; ++i)
    metaSave.blocks[blockOffsets[block] + i] = buffer[i];

  metaSave.saveCookie();
}

void gb_save_setNum(uint16_t block, int32_t number)
{
  // juggling with pointers doesn't work because of type alignments

  gb_save_set(block,(char *) &number,4);
}

void gb_save_set(uint16_t block, char *buffer)
{
  int i = 0;

  while (true)
  {
    if (buffer[i] == 0)
      break;

    metaSave.blocks[blockOffsets[block] + i] = buffer[i];
    i++;
  }

  metaSave.saveCookie();
}

int8_t gb_gui_menu(const char *title, const char **items, uint8_t len)
{
  uint8_t item = 0;

  while (true)
  {
    if (pokitto.update())
    {
      pokitto.display.setColor(WHITE,BLACK);
      pokitto.display.clear();

      pokitto.display.setCursor(1,1);

      pokitto.display.println(title);

      for (uint8_t i = 0; i < len; ++i)
      {
        uint16_t x = 4;
        uint16_t y = 10 + i * 10;

        pokitto.display.setColor(WHITE,BLACK);

        if (i == item)
        {
          pokitto.display.fillRect(x - 1,y - 1,80,9);
          pokitto.display.setColor(BLACK,WHITE);
        }

        pokitto.display.setCursor(x,y);
        pokitto.display.println(items[i]);
      }

      if (pokitto.buttons.timeHeld(BTN_DOWN) > 1)
        item += item < len - 1 ? 1 : 0;
      else if (pokitto.buttons.timeHeld(BTN_UP) > 1)
        item += item > 0 ? -1 : 0;

      if (pokitto.buttons.pressed(BTN_A))
        break;
    }
  }

  return item;
}

int16_t randRange(int16_t v1, int16_t v2)
{
  return v1 + (rand() % (v2 - v1 + 1));
}

void gb_lights_clear()
{
}

void gb_display_clear()
{
  pokitto.display.clear();
}

void gb_begin()
{
  pokitto.begin();
//  sound.ampEnable(1);
  pokitto.setFrameRate(60);
  pokitto.display.setFont(fontTiny);

  metaSave.begin("METAufoR",sizeof(metaSave),(char *) &metaSave);

//metaSave.deleteCookie();

  gb_display_fontwidth = pokitto.display.fontWidth;
  gb_display_fontheight = pokitto.display.fontHeight;
}

/*
void gb_save_config()
{
}
*/

bool gb_update()
{
  bool result = pokitto.update();

  if (result)
  {
    int16_t inc = metaModeActive != pokitto.buttons.cBtn() ? 1 : -1;

    metaLoader += inc;

    metaLoader = metaLoader < 0 ? 0 : (metaLoader > metaLoaderMax ? metaLoaderMax : metaLoader);

    if (
         (!metaModeActive && metaLoader >= metaLoaderMax) ||
         (metaModeActive && metaLoader <= 0)
       )
    {
      metaModeActive = !metaModeActive;
    }
  }

  return result;
}

void drawMetaMode()
{
  int16_t lineLength = (metaLoader * 110) / metaLoaderMax;

  if (lineLength == 0)
    return;

  if (metaModeActive)
  {
    pokitto.display.setColor(YELLOW);
    pokitto.display.drawRect(0,0,109,87);
    pokitto.display.drawRect(1,1,107,85);
  }

  pokitto.display.setColor(RED);
  pokitto.display.drawFastHLine(0,0,lineLength);
  pokitto.display.drawFastHLine(0,87,lineLength);
}

bool gb_buttons_released(uint16_t b)
{
  return pokitto.buttons.released(b);
}

bool gb_buttons_pressed(uint16_t b)
{
  return pokitto.buttons.pressed(b);
}

bool gb_buttons_repeat(uint16_t b, uint8_t t)
{
  return pokitto.buttons.repeat(b,t);
}

uint16_t gb_buttons_timeheld(uint16_t b)
{
  return pokitto.buttons.timeHeld(b);
}

void gb_display_setcolor(uint16_t c)
{
  pokitto.display.setColor(c);
}

void gb_display_setcolor(uint16_t fg, uint16_t bg)
{
  pokitto.display.setColor(fg,bg);
}

void gb_display_fill()
{
  pokitto.display.fillRect(0,0,109,87);
}

void gb_lights_fill(uint16_t c)
{
}

void gb_display_fillrect(int16_t x, int16_t y, int16_t w, int16_t h)
{
  pokitto.display.fillRect(x,y,w,h);
}

uint16_t gb_display_height()
{
  return 88; //64;
}

uint16_t gb_display_width()
{
  return 110; //80;
}

template <typename T>
void gb_display_print(T w)
{
  pokitto.display.setCursor(gb_display_cursorx,gb_display_cursory);
  pokitto.display.textWrap = gb_display_textwrap;
  pokitto.display.print(w);
}

template <typename T>
void gb_display_println(T w)
{
  pokitto.display.setCursor(gb_display_cursorx,gb_display_cursory);
  pokitto.display.textWrap = gb_display_textwrap;
  pokitto.display.println(w);
}

template <typename T>
void gb_save_set(uint16_t block, T object)
{
}

bool gb_metamode_isactive()
{
  return metaModeActive;
}

void gb_getdefaultname(char *str)
{
  str[0] = 'A';
  str[1] = 'B';
  str[2] = 'C';
  str[3] = 0;
}

void gb_gui_keyboard(const char *title, char *text, uint8_t length = 0)
{
  uint8_t pos = 0;

  while (true)
  {
    if (pokitto.update())
    {
      pokitto.display.clear();
      pokitto.display.setColor(WHITE);
      pokitto.display.setCursor(1,1);
      pokitto.display.print(title);

      for (uint8_t i = 0; i < 3; ++i)
      {
        uint16_t x = 40 + i * 8;
        pokitto.display.setCursor(x,30);
        pokitto.display.print(text[i]);

        if (i == pos)
          pokitto.display.drawFastHLine(x,38,6);
      }

      if (pokitto.buttons.timeHeld(BTN_LEFT) == 1)
        pos -= pos > 0 ? 1 : 0;
      else if (pokitto.buttons.timeHeld(BTN_RIGHT) == 1)
        pos += pos < 2 ? 1 : 0;
      else if (pokitto.buttons.timeHeld(BTN_UP) == 1 || pokitto.buttons.repeat(BTN_UP,10))
        text[pos] = 65 + ((text[pos] - 65 + 1) % 26);
      else if (pokitto.buttons.timeHeld(BTN_DOWN) == 1 || pokitto.buttons.repeat(BTN_DOWN,10))
        text[pos] = 65 + (((text[pos] > 65 ? text[pos] : 91) - 65 - 1) % 26);

      if (pokitto.buttons.pressed(BTN_A) || pokitto.buttons.pressed(BTN_B))
        break;
    }
  }
}

void gb_gui_popup_time(unsigned int value, uint8_t duration)
{
  while (duration > 0)
  {
    if (pokitto.update())
    {
      pokitto.display.clear();
      pokitto.display.setColor(BLACK,WHITE);
      pokitto.display.drawRect(2,2,105,83);
      pokitto.display.setCursor(40,30);
      pokitto.display.print("Time: ");
      pokitto.display.print(value);

      if (pokitto.buttons.pressed(BTN_A) || pokitto.buttons.pressed(BTN_B))
        break;

      duration--;
    }
  }
}

void gb_sound_playok()
{
//  sound.playOK();
}

void gb_sound_playtick()
{
//  sound.playTick();
}

void gb_display_setfontsize(uint8_t s)
{
  pokitto.display.fontSize = s;
}

void gb_display_drawbitmap(int8_t x, int8_t y, const uint8_t* bitmap, uint8_t rotation = 0, uint8_t flip = 0)
{
  uint16_t w = bitmap[0];
  uint16_t h = bitmap[1];
  uint16_t index = 0;
  
  for (int16_t j = 0; j < h; ++j)
  {
    for (int16_t i = 0; i < w; ++i)
    {
      pokitto.display.drawPixel(x + i, y + j,
        (bitmap[2 + (index / 8)] & (0b00000001 << (7 - index % 8))) ?
        pokitto.display.color : pokitto.display.bgcolor);

      ++index; 
    }
  }
}

inline void gb_display_drawpixel(int16_t x, int8_t y)
{
  pokitto.display.drawPixel(x,y);
}

uint16_t gb_createcolor(uint8_t r, uint8_t g, uint8_t b)
{
  return 0;
}

void gb_display_fillcircle(int16_t x ,int16_t y ,int16_t r)
{
  pokitto.display.fillCircle(x,y,r);
}

void gb_lights_drawpixel(int16_t x, int16_t y, uint16_t c = 0)
{
}

void gb_display_drawline(int16_t x0, int16_t y0, int16_t x1, int16_t y1)
{
  pokitto.display.drawLine(x0,y0,x1,y1);
}

void gb_sound_playpattern(const uint16_t *pattern, uint8_t channel)
{
  //pokitto.sound.playPattern(pattern,channel);
}

#endif
