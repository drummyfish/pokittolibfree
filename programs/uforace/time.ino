unsigned int timeMeasure = 0;
boolean countingTime = false;

void initTime(){
  timeMeasure = 0;
  countingTime = false;
}

void updateTime(){
  if(getTile(player.x/16, player.y/16) == 7){
    if(!countingTime){
      countingTime = true;
      timeMeasure = 0;
    }
    else{
      if(timeMeasure > 100){
      saveHighscore(timeMeasure);
      initTime();
      initPlayer();
      }
    }
  }
  if(countingTime){
    timeMeasure++;
  }
}

void drawTime(){
  gb_display_setcolor(BLACK);
  gb_display_fillrect(0, gb_display_height() - gb_display_fontheight, 4 * gb_display_fontwidth, gb_display_fontheight + 1);
  gb_display_cursorx = 0;
  gb_display_cursory = gb_display_height() - gb_display_fontheight + 1;
  gb_display_setcolor(WHITE,BLACK);
  gb_display_print(timeMeasure);
}
