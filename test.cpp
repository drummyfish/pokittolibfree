/**
  Test for PokittoLib.

  author: Miloslav Ciz
  license: CC0 1.0
*/

#include "Pokitto.h"

Pokitto::Core pokitto;

int main()
{
  pokitto.begin();

  while (pokitto.isRunning())
  {
    if (pokitto.update())
    {
      #define printFont(f,x,y,t)\
        pokitto.display.setFont(f);\
        pokitto.display.setCursor(x,y);\
        pokitto.display.print(t);
      
      printFont(fontTIC806x6,1,1,"fontTiny test 1234567890 abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!.,'-/()&@#")


    }
  }

  return 0;
}
