# PokittoLibre

![](screenshot.png)

This is a freedom-focused fork of PokittoLib, a library for an open gaming and educational console Pokitto.

Please note that I love Pokitto and by forking the library I intend to help
fix some minor licensing issues and therefore help bring more people to it.
**Support Pokitto!** <3

## about

The fork has been made in order to achive these goals:

- Remove/replace/rewrite code that is not strictly free software.
- Fix minor violations of FOSS licenses, such as marking Apache 2.0 files as modified.
- Make licensing more clear.
- Remove unneeded bloat.
- Drop the burden of supporting proprietary developments environments, since people using these won't be demanding a free library anyway.

Many unneeded files, such as the examples, were stripped. This repo may
eventually grow into a bigger thing, containing its own free examples,
extensions etc.

Differences against the original library from the user perspective:

- The start screens are visually different in order to distinguish from the original library.
- Fonts have been replaced by similarly looking CC0 clones.
- Palettes have been replaced by similarly looking CC0 clones.
- No microPython (may be added later).
- PokittoDisk doesn't work (temporarily disabled due to license). Use the other SD card libraries that are available (SDFS, PFS).
- **bug**: Overclocking (`_OSCT=2`) doesn't work at the moment.
- **bug**: Build-in onscreen FPS counter doesn't work (shows only for one frame).

original code: https://github.com/pokitto/PokittoLib

Pokitto website: https://www.pokitto.com/

## status

The liberation of the library should now be complete, everything here is supposed to be free software. The changes made were tracked and can be
found in the `issues.txt` file.

The loader is currently not a part of this project. The games can either be used without a loader (reflashing from PC), or you can use the
official one, which is however built atop some of the non-free files. This issue is to be adressed in the future, probably by including the
loader here.

To set up the official loader, go to [this thread](https://talk.pokitto.com/t/gamedisk-ready-made-sd-game-disk/566), download `start.bin`,
upload it to Pokitto and run it. That will install the loader.

## how to use

This repository focuses on using [GNU ARM Embedded Toolchain](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm) for building
the programs. There is a Makefile you can use for that -- the instruction on usage are contained in the Makefile.

This version of the library should be usable with other environments as well, but weren't tested to and don't focus on them. You should
be able to just replace the `Pokitto` subfolder of the original PokittoLib with `PokittoLib` subfolder from this repository -- don't
forget to rename it to just `Pokitto`! The names of the folders differ  -- and all should just work.

To upload `firmware.bin` to Pokitto, you can use the `upload.sh` script.

In your program you can distiguish between PokittoLibre and the official library at compile time. If PokittoLibre is used,
`POKITTO_LIBRE` will be defined as `1`.

## license

Unfortunately due to the nature of the original library there isn't a single license for the whole codebase as the code comes from
various sources. However, unlike with the original library, all code here should be licensed as free and open. Here is a quick
summary of the licenses:

- All changes, rewrites and additions made by PokittoLibre project are released under CC0 1.0. This is stated directly in the files.
- Most of the PokittoLib original source is BSD licensed.
- The folder mbed-pokitto contains mostly files licensed Apache 2.0.
- Some of the source code (such as a few fonts) has been taken from Gamebuino library and is LGPL licensed.
- Non-core libraries are under various licenses, e.g. FixMath is MIT licensed.
