PetitFS module

license: All source files in this directory are free-licensed under an MIT-like
license. This is mentioned in pff.h and pff.cpp files and clearly states it
applies to the whole PFFS module, so this includes the other files as well, even
though they don't have an explicit license statement in them.
