/*
  This file has been rewritten from scratch by Drummyfish and is placed in
  public domain under CC0 1.0 waiver.
*/

#include <stdint.h>
#include <iap.h>

#include "LPC11U6x.h"
#include "PokittoDisk.h"

#define EEPROM_FUNC_ADDRESS 0x1fff1ff1 // this is where the function is

typedef int (*EEPROMFunc)(unsigned int[], unsigned int[]);

int uploadPageToFlash(uint32_t addr, uint8_t* data)
{
  unsigned int params[5], result[4];
  int clear = 0;
  int first = 0;
  unsigned int sector;
  unsigned int page;

  EEPROMFunc func = (EEPROMFunc) EEPROM_FUNC_ADDRESS;

  #define callFunc(p1,p2,p3,p4,p5)\
  {\
    params[0] = p1;\
    params[1] = p2;\
    params[2] = p3;\
    params[3] = p4;\
    params[4] = p5;\
    func(params,result);\
    if (result[0])\
      return 1;\
  }

  __disable_irq(); // disable interrupt

  if (addr < 0x18000)
    sector = addr / 0x1000;
  else if (addr >= 0x38000)
    sector = 28;
  else if (addr >= 0x30000)
    sector = 27;
  else if (addr >= 0x28000)
    sector = 26;
  else if (addr >= 0x20000)
    sector = 25;
  else
    sector = 24;

  first = (addr & (sector >= 24 ? 0x7FFF : 0x0FFF)) == 0;
  
  callFunc(50,sector,sector,0,0)

  if (addr == 0x39000)
    clear = 1;

  unsigned long khzRate = SystemCoreClock / 1000;

  if (first)
  {
    callFunc(52,sector,sector,khzRate,0)
    callFunc(50,sector,sector,0,0)
  }

  if (clear)
  {
    callFunc(59,896,1023,khzRate,0
)
    callFunc(50,sector,sector,0,0)
  }

  callFunc(51,(uint32_t) (uint32_t *) addr, (uint32_t) data, 0x100, khzRate)

  __enable_irq(); // reenable interrupt

  return 0;

  #undef callFunc
}

#define callEEPROM(commandNo)\
  EEPROMFunc func = (EEPROMFunc) EEPROM_FUNC_ADDRESS;\
  unsigned int params[5], result[4];\
  params[0] = commandNo;\
  params[1] = (uint32_t) addr;\
  params[2] = (uint32_t) data;\
  params[3] = n;\
  params[4] = SystemCoreClock / 1000; /* in kHz */\
  __disable_irq(); /* disable interrups */\
  func(params,result);\
  __enable_irq(); /* reenable interrupts */\
 if (result[0])\
   while(1);

__attribute__((section(".IAP_Code"))) void writeEEPROM(uint16_t* addr, uint8_t* data, uint32_t n)
{
  callEEPROM(61)
}

__attribute__((section(".IAP_Code"))) void readEEPROM(uint16_t* addr, uint8_t* data, uint32_t n)
{
  callEEPROM(62)
}

#undef callEEPROM

uint8_t readFromEEPROM(uint16_t* index)
{
  uint8_t value;
  readEEPROM(index,&value,1);
  return value;
}

void writeToEEPROM(uint16_t* index, uint8_t value)
{
  writeEEPROM(index,&value,1);
}

