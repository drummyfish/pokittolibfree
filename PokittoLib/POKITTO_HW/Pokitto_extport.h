#ifndef EXTPORT_H
#define EXTPORT_H

/*
  This file has been rewritten from scratch by Drummyfish. It is placed in
  public domain under CC0 1.0 waiver.
*/

#include "mbed.h"

#if POK_BOARDREV != 0

  #define EXT0           P1_19
  #define EXT0_PIN       0x13
  #define EXT0_PORT      0x01

  #define EXT1           P0_11
  #define EXT1_PIN       0x0B
  #define EXT1_PORT      0x00

  #define EXT2           P0_12
  #define EXT2_PIN       0x0C
  #define EXT2_PORT      0x00

  #define EXT3           P0_13
  #define EXT3_PIN       0x0D
  #define EXT3_PORT      0x00

  #define EXT4           P0_14
  #define EXT4_PIN       0x0E
  #define EXT4_PORT      0x00

  #define EXT5           P0_17
  #define EXT5_PIN       0x11
  #define EXT5_PORT      0x00

  #define EXT6           P0_18
  #define EXT6_PIN       0x12
  #define EXT6_PORT      0x00

  #define EXT7           P0_19
  #define EXT7_PIN       0x13
  #define EXT7_PORT      0x00

  #define EXT8           P1_20
  #define EXT8_PIN       0x14
  #define EXT8_PORT      0x01

  #define EXT9           P1_21
  #define EXT9_PIN       0x15
  #define EXT9_PORT      0x01

  #define EXT10          P1_22
  #define EXT10_PIN      0x16
  #define EXT10_PORT     0x01

  #define EXT11          P1_23
  #define EXT11_PORT     0x01
  #define EXT11_PIN      0x17

  #define EXT12          P1_5
  #define EXT12_PIN      0x05
  #define EXT12_PORT     0x01

  #define EXT13          P1_6
  #define EXT13_PIN      0x06
  #define EXT13_PORT     0x01

  #define EXT14          P1_8
  #define EXT14_PIN      0x08
  #define EXT14_PORT     0x01

  #define EXT15          P1_26
  #define EXT15_PIN      0x13
  #define EXT15_PORT     0x00

  #define EXT16          P1_27
  #define EXT16_PIN      0x0D
  #define EXT16_PORT     0x00

  #define EXT17          P0_16
  #define EXT17_PIN      0x10
  #define EXT17_PORT     0x00

#else
// POK_BOARDREV == 1

  #define EXT0_PIN       0x13
  #define EXT0_PORT      0x01

  #define EXT1_PIN       0x0B
  #define EXT1_PORT      0x00

  #define EXT2_PIN       0x0C
  #define EXT2_PORT      0x00

  #define EXT3_PIN       0x0D
  #define EXT3_PORT      0x00

  #define EXT4_PIN       0x0E
  #define EXT4_PORT      0x00

  #define EXT5_PIN       0x11
  #define EXT5_PORT      0x00

  #define EXT6_PIN       0x12
  #define EXT6_PORT      0x00

  #define EXT7_PIN       0x13
  #define EXT7_PORT      0x00

  #define EXT8_PIN       0x14
  #define EXT8_PORT      0x01

  #define EXT9_PIN       0x15
  #define EXT9_PORT      0x01

  #define EXT10_PIN      0x16
  #define EXT10_PORT     0x01

  #define EXT11_PIN      0x17
  #define EXT11_PORT     0x01

  #define EXT12_PIN      0x05
  #define EXT12_PORT     0x01

  #define EXT13_PIN      0x06
  #define EXT13_PORT     0x01

  #define EXT14_PIN      0x08
  #define EXT14_PORT     0x01

  #define EXT15_PIN      0x13
  #define EXT15_PORT     0x00

  #define EXT16_PIN      0x0D
  #define EXT16_PORT     0x00

  #define EXT17_PIN      0x10
  #define EXT17_PORT     0x00

#endif 

#endif // guard
