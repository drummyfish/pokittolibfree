/*
  This file has been rewritten from scratch by Drummfish and is placed in
  public domain under CC0 1.0 waiver.
*/

#include "timer.h"
#include "clock.h"

void timerInit(LPCTimer *timer)
{
  LPCClock clock = SysClock32B0;

  if (timer == LPCTimer16A)
    clock = SysClock16B0;
  else if (timer == LPCTimer16B)
    clock = SysClock16B1;
  else if (timer == LPCTimer32B)
    clock = SysClock32B1;
  else
    clock = SysClock32B0;

  enablePeriphClock(clock);
}

void timerReset(LPCTimer *timer)
{
  uint32_t backup = timer->timerControl;

  timer->timerControl = 0;
  timer->timerCounter = 1;

  timer->timerControl = 0x02;

  while (timer->timerCounter != 0);

  timer->timerControl = backup;
}

