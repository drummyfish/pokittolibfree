/*
  This file has been rewritten from scratch by Drummyfish. It is placed in
  public domain under CC0 1.0 waiver.
*/

#include "Pokitto.h"

#if POK_USE_EXT >= 1
  void ext_write(uint32_t v)
  {
    #define check(n) if (v & 0x01) SET_EXT##n else CLR_EXT#n; v = v >> 1;

    check(0)
    check(1)
    check(2)
    check(3)
    check(4)
    check(5)
    check(6)
    check(7)
    check(8)
    check(9)
    check(10)
    check(11)
    check(12)
    check(13)
    check(14)
    check(15)
    check(16)
    check(17)

    #undef check
  }

  AnalogIn   ext0(P1_19);
  DigitalIn  ext1(P0_11, PullUp);

  DigitalOut ext2(P0_12);
  DigitalOut ext3(P0_13);
  DigitalOut ext4(P0_14);
  DigitalOut ext5(P0_17);
  DigitalOut ext6(P0_18);
  DigitalOut ext7(P0_19);
  DigitalOut ext8(P1_20);
  DigitalOut ext9(P1_21);
  DigitalOut ext10(P1_22);
  DigitalOut ext11(P1_23);
  DigitalOut ext12(P1_5);
  DigitalOut ext13(P1_6);
  DigitalOut ext14(P1_8);
  DigitalOut ext15(P1_26);
  DigitalOut ext16(P1_27);
  DigitalOut ext17(P0_16);

#else

  void ext_write(uint32_t v)
  {
  }

#endif
