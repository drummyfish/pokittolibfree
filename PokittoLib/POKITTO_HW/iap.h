/*
  This file has been rewritten from scratch by Drummyfish and is placed in
  public domain under CC0 1.0 waiver.
*/

#ifndef IAP_H
#define IAP_H

#include "Pokitto_settings.h"

#define IAP_STA_CMD_SUCCESS                               0x00
#define IAP_STA_INVALID_COMMAND                           0x01
#define IAP_STA_SRC_ADDR_ERROR                            0x02
#define IAP_STA_DST_ADDR_ERROR                            0x03
#define IAP_STA_SRC_ADDR_NOT_MAPPED                       0x04
#define IAP_STA_DST_ADDR_NOT_MAPPED                       0x05
#define IAP_STA_COUNT_ERROR                               0x06
#define IAP_STA_INVALID_SECTOR                            0x07
#define IAP_STA_SECTOR_NOT_BLANK                          0x08
#define IAP_STA_SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION   0x09
#define IAP_STA_COMPARE_ERROR                             0x0A
#define IAP_STA_BUSY                                      0x0B
#define IAP_STA_INVALD_PARAM                              0x0C

extern int uploadPageToFlash(uint32_t addr, uint8_t *data);
extern void writeToEEPROM(uint16_t *index, uint8_t value);
extern uint8_t readFromEEPROM(uint16_t *index);

#endif
