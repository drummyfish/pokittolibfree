/*
  This file has been rewritten from scratch by Drummyfish and is placed
  in public domain under CC0 1.0 waiver.
*/

#ifndef CLOCK_11U6X_H
#define CLOCK_11U6X_H

#include "lpc_defs.h"

#ifdef __cplusplus // this need to be here for the linker
extern "C"
{
#endif

typedef int LPCClock;

#define SysClock16B0 7
#define SysClock16B1 8
#define SysClock32B0 9
#define SysClock32B1 10

uint32_t getClockRate();

static inline void enablePeriphClock(LPCClock clock)
{
  LPCSysCont->clockControlReg =
    (1 << clock) | LPCSysCont->clockControlReg;
}

#ifdef __cplusplus // for the linker
}
#endif

#endif // guard
