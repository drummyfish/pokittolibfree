/*
  This file has been rewritten from scratch by Drummfish and is placed in
  public domain under CC0 1.0 waiver.
*/

#ifndef TIMER_11U6X_H
#define TIMER_11U6X_H

#include <stdint.h>

#ifdef __cplusplus // needs to be here for the linker
extern "C"
{
#endif

typedef struct
{
  volatile uint32_t       interruptReg;
  volatile uint32_t       timerControl;
  volatile uint32_t       timerCounter;
  volatile uint32_t       prescaleReg;
  volatile uint32_t       prescaleControl;
  volatile uint32_t       matchControl;
  volatile uint32_t       matchReg[4];
  volatile uint32_t       captureControl;
  volatile uint32_t       captureReg[4];
  volatile uint32_t       externMatch;
  volatile const uint32_t nothing[12];
  volatile uint32_t       countControl;
  volatile uint32_t       PWMC;
} LPCTimer;

static inline uint8_t timerMatchPending(LPCTimer *timer, int8_t number)
{
  return (uint8_t) ((timer->interruptReg & ( 0x0f & (1 << number))) != 0);
}

static inline void timerClearMatch(LPCTimer *timer, int8_t number)
{
  timer->interruptReg = 1 << number;
}

static inline void timerEnable(LPCTimer *timer)
{
  timer->timerControl |= (uint32_t) 0x01;
}

static inline void timerMatchEnableInt(LPCTimer *timer, int8_t number)
{
  timer->matchControl = timer->matchControl | (1 << (3 * number));
}

static inline void timerSetMatch(LPCTimer *timer, int8_t number, uint32_t value)
{
  timer->matchReg[number] = value;
}

static inline void timerResetOnMatchEnable(LPCTimer *timer, int8_t number)
{
  timer->matchControl = timer->matchControl | (1 << (1 + (3 * number)));
}

void timerInit(LPCTimer *timer);

void timerReset(LPCTimer *timer);

#ifdef __cplusplus // extern "C", for the linker
}
#endif

#endif // guard

