/*
  This file has been rewritten from scratch by Drummyfish and is placed in
  public domain under CC0 1.0 waiver.
*/

#ifndef LPC_DEFS_H
#define LPC_DEFS_H

#include "timer.h"
#include <stdint.h>

typedef struct
{
  // we don't need this stuff but it needs to be here for the alignment
  volatile uint32_t stuff[32];

  volatile uint32_t clockControlReg; // only thing we'll ever use

  // the rest of filler stuff
  volatile uint32_t stuff2[221];
} LPCSystemControl;

#define LPCSysCont  ((LPCSystemControl *) 0x40048000)

#define LPCTimer32A ((LPCTimer *) 0x40014000)
#define LPCTimer32B ((LPCTimer *) 0x40018000)
#define LPCTimer16A ((LPCTimer *) 0x4000C000)
#define LPCTimer16B ((LPCTimer *) 0x40010000)

#endif // guard

