/*
  This file has been rewritten from scratch by Drummyfish and is placed
  in public domain under CC0 1.0 waiver.
*/

#include "clock.h"

uint32_t getClockRate()
{
  return 48000000;
}
