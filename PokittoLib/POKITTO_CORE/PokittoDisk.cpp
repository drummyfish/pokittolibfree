/*
  This file has been rewritten from scratch by Drummyfish and is placed in
  public domain under CC0 1.0 waiver.

  This file has been temporarily emptied, because it wasn't freely licensed,
  and DOES NOTHING at the moment. Once this file is assigned a proper FOSS
  license in the original repo, it should be placed here!
*/

#include "Pokitto.h"

char emptyName[] = "";

char currentfile[15];

PFFS::FATFS   fs;
PFFS::BYTE    res;
PFFS::FATDIR  dir;
PFFS::FILINFO fno;

int pokInitSD()                                { return 0;         }
uint8_t fileOpen(char*, char)                  { return 0;         }
void fileClose()                               {                   }
char fileGetChar()                             { return 0;         }
void filePutChar(char)                         {                   }
void fileWriteBytes(uint8_t *, uint16_t)       {                   }
uint16_t fileReadBytes(uint8_t *, uint16_t)    { return 0;         }
void fileSeekAbsolute(long)                    {                   }
void fileSeekRelative(long)                    {                   }
void fileRewind()                              {                   }
void fileEnd()                                 {                   }
long int fileGetPosition()                     {                   }
uint8_t filePeek(long)                         { return 0;         }
void filePoke(long, uint8_t)                   {                   }
int fileReadLine(char*,int)                    { return 0;         }
char* getCurrentFileName ()                    { return emptyName; }
char* getNextFile (char*)                      { return emptyName; }
char* getNextFile ()                           { return emptyName; }
char* getFirstFile(char*)                      { return emptyName; }
char* getFirstFile()                           { return emptyName; }
char* getFirstDirEntry()                       { return emptyName; }
char* getNextDirEntry()                        { return emptyName; }
int isThisFileOpen(char*)                      { return 0;         }
int fileOK()                                   { return 0;         }
int dirOpen()                                  { return 0;         }
int dirUp()                                    { return 0;         }
