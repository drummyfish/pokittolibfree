#ifndef PALETTES_H
#define PALETTES_H

/*
  This file has been rewritten from scratch by Drummyfish and is placed in
  public domain under CC0 1.0 waiver.
*/

#include "Pokitto_settings.h"
#define POK_DEFAULT_PALETTE paletteCGA

#define T extern const unsigned char

T paletteDefault[];

T paletteMono[];
T paletteZXSpec[];
T paletteCGA[];
T paletteGameboy[];
T paletteCopper[];
T paletteNeon[];
T paletteMagma[];
T palettePico[];
T paletteDB16[];
T paletteRainbow[];
T paletteAction[];

#undef T

#endif
