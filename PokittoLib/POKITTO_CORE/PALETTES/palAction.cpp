/*
  This palette file has been rewritten from scratch by Drummyfish. The colors
  were chosen to be similar, by visually picking similarly looking colors in
  a graphic editor. The color order has to be kept for compatibility reasons.

  This file is put in public domain under CC0 1.0 waiver.
*/

#include "PokittoPalettes.h"

const unsigned char paletteAction[48] =
{
  0x0a, 0x07, 0x1f,
  0x14, 0x00, 0x5e,
  0x2c, 0x12, 0xaf,
  0x43, 0x39, 0xcf,
  0x5f, 0xc3, 0x5a,
  0xec, 0xc4, 0x1c,
  0xb7, 0x62, 0x19,
  0x8f, 0x1c, 0x12,
  0x1f, 0x20, 0x37,
  0x34, 0x35, 0x52,
  0x48, 0x4e, 0x6b,
  0x5c, 0x68, 0x9f,
  0x81, 0x8e, 0xca,
  0x6c, 0xa3, 0xcf,
  0x6a, 0xdb, 0xec,
  0xd3, 0xff, 0xff  
};
