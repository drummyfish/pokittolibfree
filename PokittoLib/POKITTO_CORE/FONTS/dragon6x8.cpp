/*
  This font file has been rewritten by Drummyfish. It replaces the original
  proprietary font with a custom made clone. This file and the font clone
  are placed in the public domain under CC0 1.0 waiver.
*/

#include "PokittoFonts.h"

const unsigned char fontDragon[] =
{
  6, 8, 32, 0, // width, height, start char, caps only
  0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,       // 32: ' '
  0x02, 0x58, 0x07, 0x00, 0x00, 0x00, 0x00,       // 33: '!'
  0x03, 0x03, 0x00, 0x07, 0x00, 0x00, 0x00,       // 34: '"'
  0x06, 0x64, 0x3E, 0x24, 0x74, 0x1E, 0x04,       // 35: '#'
  0x05, 0x04, 0x4A, 0xFF, 0x2A, 0x10, 0x00,       // 36: '$'
  0x06, 0x66, 0x16, 0x08, 0x68, 0x64, 0x02,       // 37: '%'
  0x06, 0x6C, 0x52, 0x22, 0x58, 0x48, 0x20,       // 38: '&'
  0x01, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00,       // 39: '''
  0x03, 0x38, 0x44, 0x02, 0x00, 0x00, 0x00,       // 40: '('
  0x03, 0x40, 0x22, 0x1C, 0x00, 0x00, 0x00,       // 41: ')'
  0x05, 0x2A, 0x1C, 0x3E, 0x1C, 0x2A, 0x00,       // 42: '*'
  0x05, 0x08, 0x08, 0x3E, 0x08, 0x08, 0x00,       // 43: '+'
  0x02, 0xC0, 0x60, 0x00, 0x00, 0x00, 0x00,       // 44: ','
  0x04, 0x08, 0x08, 0x08, 0x08, 0x00, 0x00,       // 45: '-'
  0x02, 0x60, 0x60, 0x00, 0x00, 0x00, 0x00,       // 46: '.'
  0x06, 0x60, 0x10, 0x08, 0x08, 0x04, 0x02,       // 47: '/'
  0x03, 0x7C, 0x42, 0x3E, 0x00, 0x00, 0x00,       // 48: '0'
  0x02, 0x44, 0x7E, 0x00, 0x00, 0x00, 0x00,       // 49: '1'
  0x03, 0x70, 0x4A, 0x46, 0x00, 0x00, 0x00,       // 50: '2'
  0x03, 0x60, 0x4A, 0x36, 0x00, 0x00, 0x00,       // 51: '3'
  0x03, 0x1E, 0x50, 0x7E, 0x00, 0x00, 0x00,       // 52: '4'
  0x03, 0x4E, 0x2A, 0x12, 0x00, 0x00, 0x00,       // 53: '5'
  0x03, 0x38, 0x54, 0x62, 0x00, 0x00, 0x00,       // 54: '6'
  0x03, 0x72, 0x0A, 0x06, 0x00, 0x00, 0x00,       // 55: '7'
  0x03, 0x74, 0x4A, 0x36, 0x00, 0x00, 0x00,       // 56: '8'
  0x03, 0x44, 0x2A, 0x1E, 0x00, 0x00, 0x00,       // 57: '9'
  0x02, 0x6C, 0x6C, 0x00, 0x00, 0x00, 0x00,       // 58: ':'
  0x02, 0xEC, 0x6C, 0x00, 0x00, 0x00, 0x00,       // 59: ';'
  0x05, 0x10, 0x28, 0x28, 0x44, 0x44, 0x00,       // 60: '<'
  0x03, 0x14, 0x14, 0x14, 0x00, 0x00, 0x00,       // 61: '='
  0x05, 0x44, 0x44, 0x28, 0x28, 0x10, 0x00,       // 62: '>'
  0x04, 0x02, 0x51, 0x09, 0x07, 0x00, 0x00,       // 63: '?'
  0x06, 0x38, 0x44, 0x52, 0x5A, 0x12, 0x0C,       // 64: '@'
  0x05, 0xC9, 0x7F, 0x09, 0x11, 0x7E, 0x00,       // 65: 'A'
  0x05, 0x82, 0xFF, 0x49, 0x4E, 0x30, 0x00,       // 66: 'B'
  0x04, 0x3E, 0x41, 0x21, 0x13, 0x00, 0x00,       // 67: 'C'
  0x03, 0x7F, 0x41, 0x3F, 0x00, 0x00, 0x00,       // 68: 'D'
  0x04, 0xC9, 0x7F, 0x45, 0x41, 0x00, 0x00,       // 69: 'E'
  0x04, 0xC9, 0x7F, 0x05, 0x05, 0x00, 0x00,       // 70: 'F'
  0x04, 0x3E, 0x41, 0x29, 0x1B, 0x00, 0x00,       // 71: 'G'
  0x05, 0xC8, 0x7F, 0x04, 0x7F, 0x01, 0x00,       // 72: 'H'
  0x03, 0xC1, 0x7F, 0x41, 0x00, 0x00, 0x00,       // 73: 'I'
  0x04, 0x60, 0x41, 0x3F, 0x02, 0x00, 0x00,       // 74: 'J'
  0x05, 0xC0, 0x7F, 0x04, 0x4B, 0x31, 0x00,       // 75: 'K'
  0x04, 0x7F, 0x21, 0x20, 0x40, 0x00, 0x00,       // 76: 'L'
  0x06, 0xC1, 0x7F, 0x02, 0x3C, 0x02, 0x7F,       // 77: 'M'
  0x06, 0xC0, 0x7F, 0x02, 0x0C, 0x10, 0x7F,       // 78: 'N'
  0x04, 0x7E, 0x41, 0x41, 0x3F, 0x00, 0x00,       // 79: 'O'
  0x05, 0xC2, 0x7F, 0x09, 0x05, 0x03, 0x00,       // 80: 'P'
  0x05, 0x7E, 0x41, 0x21, 0x7F, 0xC0, 0x00,       // 81: 'Q'
  0x05, 0xC2, 0x7F, 0x09, 0x35, 0x43, 0x00,       // 82: 'R'
  0x05, 0x66, 0x29, 0x49, 0x4A, 0x30, 0x00,       // 83: 'S'
  0x06, 0x03, 0xC1, 0x7F, 0x02, 0x02, 0x01,       // 84: 'T'
  0x05, 0x3F, 0x40, 0x40, 0x3F, 0x21, 0x00,       // 85: 'U'
  0x05, 0x01, 0x3F, 0x40, 0x20, 0x1F, 0x00,       // 86: 'V'
  0x06, 0xC1, 0x7F, 0x40, 0x38, 0x40, 0x3F,       // 87: 'W'
  0x06, 0x46, 0x63, 0x54, 0x08, 0x14, 0x63,       // 88: 'X'
  0x04, 0x0F, 0x48, 0x24, 0x1F, 0x00, 0x00,       // 89: 'Y'
  0x05, 0xF1, 0x49, 0x25, 0x25, 0x43, 0x00,       // 90: 'Z'
  0x03, 0x70, 0x4E, 0x02, 0x00, 0x00, 0x00,       // 91: '['
  0x06, 0x02, 0x04, 0x08, 0x08, 0x10, 0x60,       // 92: '\'
  0x03, 0x40, 0x72, 0x0E, 0x00, 0x00, 0x00,       // 93: ']'
  0x04, 0x06, 0x01, 0x02, 0x04, 0x00, 0x00,       // 94: '^'
  0x05, 0x40, 0x40, 0x40, 0x40, 0x40, 0x00,       // 95: '_'
  0x02, 0x01, 0x06, 0x00, 0x00, 0x00, 0x00,       // 96: '`'
  0x05, 0x30, 0x50, 0x54, 0x68, 0x50, 0x00,       // 97: 'a'
  0x03, 0x7F, 0x48, 0x3C, 0x00, 0x00, 0x00,       // 98: 'b'
  0x03, 0x38, 0x44, 0x24, 0x00, 0x00, 0x00,       // 99: 'c'
  0x03, 0x78, 0x44, 0x3F, 0x00, 0x00, 0x00,       // 100: 'd'
  0x04, 0x38, 0x54, 0x54, 0x0C, 0x00, 0x00,       // 101: 'e'
  0x03, 0x48, 0x3F, 0x05, 0x00, 0x00, 0x00,       // 102: 'f'
  0x04, 0x4C, 0x94, 0xA4, 0x78, 0x00, 0x00,       // 103: 'g'
  0x04, 0x7F, 0x08, 0x44, 0x38, 0x00, 0x00,       // 104: 'h'
  0x03, 0x44, 0x7D, 0x20, 0x00, 0x00, 0x00,       // 105: 'i'
  0x03, 0xC4, 0x7D, 0x20, 0x00, 0x00, 0x00,       // 106: 'j'
  0x04, 0x7F, 0x10, 0x6A, 0x04, 0x00, 0x00,       // 107: 'k'
  0x02, 0x7E, 0xC0, 0x00, 0x00, 0x00, 0x00,       // 108: 'l'
  0x05, 0x7C, 0x04, 0x1C, 0xC4, 0x78, 0x00,       // 109: 'm'
  0x04, 0x7C, 0x04, 0xC4, 0x78, 0x00, 0x00,       // 110: 'n'
  0x03, 0x78, 0x44, 0x3C, 0x00, 0x00, 0x00,       // 111: 'o'
  0x03, 0xF8, 0x24, 0x1C, 0x00, 0x00, 0x00,       // 112: 'p'
  0x03, 0x38, 0x24, 0xFC, 0x00, 0x00, 0x00,       // 113: 'q'
  0x03, 0x7E, 0x04, 0x0C, 0x00, 0x00, 0x00,       // 114: 'r'
  0x04, 0xC8, 0x54, 0x54, 0x20, 0x00, 0x00,       // 115: 's'
  0x03, 0x04, 0x7F, 0x42, 0x00, 0x00, 0x00,       // 116: 't'
  0x04, 0xFC, 0x40, 0x40, 0x3C, 0x00, 0x00,       // 117: 'u'
  0x04, 0x7C, 0x40, 0x22, 0x1C, 0x00, 0x00,       // 118: 'v'
  0x06, 0x7C, 0x40, 0x38, 0x40, 0x22, 0x1C,       // 119: 'w'
  0x05, 0x44, 0x2C, 0x10, 0x68, 0x44, 0x00,       // 120: 'x'
  0x04, 0x3C, 0xA0, 0x90, 0x7C, 0x00, 0x00,       // 121: 'y'
  0x05, 0x64, 0x54, 0x54, 0x4C, 0xC4, 0x00,       // 122: 'z'
  0x04, 0x08, 0x38, 0x46, 0x02, 0x00, 0x00,       // 123: '{'
  0x02, 0xF0, 0x0F, 0x00, 0x00, 0x00, 0x00,       // 124: '|'
  0x04, 0x40, 0x62, 0x1C, 0x10, 0x00, 0x00,       // 125: '}'
  0x04, 0x18, 0x08, 0x10, 0x18, 0x00, 0x00,       // 126: '~'
  0x06, 0xFF, 0x81, 0x81, 0x81, 0x81, 0xFF        // 127: ' '
};
