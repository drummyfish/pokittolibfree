#ifndef FILE_IO_H
#define FILE_IO_H

/*
  This file has been rewritten from scratch by Drummyfish and is placed in
  public domain under CC0 1.0 waiver.
*/

#include <stdint.h>

#define FILE_MODE_ASCII     0
#define FILE_MODE_READWRITE 0
#define FILE_MODE_OVERWRITE 0
#define FILE_MODE_APPEND    1
#define FILE_MODE_READONLY  2
#define FILE_MODE_BINARY    4
#define FILE_MODE_FAILED    8

extern int pokInitSD();

extern unsigned char fileOpen(char *filename, char mode);
extern void fileClose();
extern int fileOK();

extern long int getFileLength();
extern char *getCurrentFileName();
extern int isThisFileOpen(char *file);

extern char fileGetChar();
extern int fileReadLine(char *buffer, int num);
extern void filePutChar(char c);
extern uint16_t fileReadBytes(uint8_t *buffer, uint16_t num);
extern void fileWriteBytes(uint8_t *buffer, uint16_t num);

extern long int fileGetPosition();
extern void fileSeekRelative(long int a);
extern void fileSeekAbsolute(long int a);
extern uint8_t filePeek(long int a);
extern void fileEnd();
extern void fileRewind();

extern void filePoke(long int a, uint8_t b);

#endif
