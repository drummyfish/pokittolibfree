All files in this folder are free-licensed under an MIT-like license, which is
stated in ff.h and ff.cpp. The statement includes the whole module, so it
also applies to other files, which do not contain an explicit license statement.
