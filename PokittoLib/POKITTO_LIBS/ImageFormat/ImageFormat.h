/**
  This file has been rewritten from scratch by Drummyfish.

  It is released into the public domain under the CC0 1.0 waiver.
*/

#ifndef IMAGEFORMAT_H
#define IMAGEFORMAT_H

#define POK_TRACE(what)\
  printf("trace: %s\n",what)

extern int directDrawImageFileFromSD(
  int16_t sx,
  int16_t sy,
  char *filePath);

extern int directDrawImageFileFromSD(
  uint16_t ix,
  uint16_t iy,
  uint16_t iw,
  uint16_t ih,
  int16_t sx,
  int16_t sy,
  char* filePath);

extern int openImageFileFromSD(
  char *filePath,
  uint16_t **paletteOut,
  uint8_t **bitmapOut);

#endif
