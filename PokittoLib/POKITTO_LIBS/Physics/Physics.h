#ifndef POKPHYSICS_H
#define POKPHYSICS_H

/*
  This file has been rewritten from scratch by Drummyfish and is placed in
  public domain under CC0 1.0 waiver.
*/

#ifndef PHYSICS_H
#define PHYSICS_H

#include <vector>
#include <cfloat>
#include <stdint.h>

#include "Scene.h"
#include "Collision.h"
#include "Manifold.h"
#include "Shape.h"
#include "Clock.h"
#include "Body.h"
#include "IEMath.h"

#endif
