/*
  This file has been rewritten from scratch by Drummyfish.

  It is placed in the public domain under CC0 1.0 waiver.
*/

#ifndef SYNTH_H
#define SYNTH_H

#include "Synth_osc.h"
#include "Synth_song.h"

#define RBTRACKER_VERSION 0.03f

#define ARPTICK (POK_AUD_FREQ / 441)
#define VOLTICK (POK_AUD_FREQ / 8820)

#define OVERDRIVE       4

#define MAX_WAVETYPES   6
#define MAX_ARPMODE     16

#define WOOF            0
#define WSQUARE         1
#define WSAW            2
#define WTRI            3
#define WNOISE          4
#define WSAMPLE         5
#define WPNOISE         6

#define OPT_ADSR        1
#define OPT_LOOP        2
#define OPT_ECHO        3
#define OPT_OVERDRIVE   4
#define OPT_NORMALIZE   16

#define ARPSTEPMAX      4
#define PATTERNLENGTH   64
#define MAXPATTERNS     10
#define MAXBLOCKS       30

#define NUMWAVES        5
#define NUMMIXES        4
#define NUMENVELOPES    3

typedef void (*waveFunction)(OSC*);
typedef void (*envFunction)(OSC*);
typedef void (*mixFunction)();
typedef void (*streamsFunction)();

extern mixFunction HWMarr[];
extern envFunction Earr[];
extern waveFunction Farr[];
extern mixFunction Marr[];

extern bool track1on, track2on, track3on, playing, tableRefresh;
extern uint16_t playerpos;
extern long int samplesperpattern;
extern uint16_t notetick, samplespertick;
extern long int writeindex, readindex;
extern uint8_t tick, sequencepos;
extern int8_t arptable[][5];
extern SONG song;
extern OSC osc1, osc2, osc3;
extern OSC patch[];
extern BLOCK block[];
extern uint16_t reqs[];
extern uint32_t const cincs[];
extern uint16_t noiseval;

extern void initAudio();
extern void startSound();
extern void stopSound();
extern void playNote(uint8_t pitch, uint8_t duration, uint8_t channel);
extern void testOsc();
extern void terminateSound();
extern void killSound();
extern void updatePlayback();
extern void updatePlaybackSD(uint8_t a);
extern void emptyOscillators();
extern void emptyPatches();
extern void emptyBlocks();
extern void emptySong();
extern void registerStreamsCallback(streamsFunction);
extern uint8_t xorshift8();
extern uint16_t xorshift16();
extern void setVolume(int volume);
extern void setWave(int wave);
extern void makeSampleInstruments();
extern void setPitch(int pitch);
extern void initStreams(uint8_t a);
extern void writeChunkToSD(uint8_t *chunk);
extern void readChunkFromSD(uint8_t *chunk);
extern streamsFunction streamCallbackPtr;
extern int openSongFromSD(char *fileName);
extern void waveoff(OSC* o);

extern void setOSC(
  OSC *o,
  uint8_t on,
  uint8_t wave,
  uint8_t loop,
  uint8_t echo,
  uint8_t adsr,
  uint8_t notenumber,
  uint16_t volume,
  uint16_t attack,
  uint16_t decay,
  uint16_t sustain,
  uint16_t release,
  int16_t maxbend,
  int16_t bendrate,
  uint8_t arpmode,
  uint8_t overdrive,
  uint8_t kick);

extern void setOSC( // parameter names unknown
  OSC* p,
  uint8_t a,
  uint8_t b,
  uint16_t c,
  uint8_t d,
  uint32_t e);

#endif
