/*
  This file has been rewritten by Drummyfish.

  It is placed in the public domain under CC0 1.0 waiver.
*/

#ifndef SYNTHSONG_H
#define SYNTHSONG_H

#define CHUNKSIZE 48

struct BLOCK
{
  uint8_t instrument[64];
  uint8_t notenumber[64];
};

struct SONG
{
  uint8_t rb_version;

  uint8_t song_end;
  uint8_t num_patches;
  uint8_t num_patterns;
  uint8_t num_channels;

  const uint8_t *note_stream[3];
  const uint8_t *instrument_stream[3];

  uint16_t song_bpm;

  int8_t song_loop;

  uint8_t block_sequence[3][10];
};

#endif
