/*
  This file has been rewritten from scratch by Drummyfish.

  It is placed in the public domain under CC0 1.0 waiver.
*/

#ifndef SYNTHOSC_H
#define SYNTHOSC_H

#include "Pokitto.h"

typedef uint8_t byte;

struct OSC
{
  uint8_t on;
  uint8_t loop;
  uint8_t echo;
  uint8_t echodiv;

  uint16_t vol;

  uint8_t wave;
  uint8_t adsr;
  uint8_t tonic;
  uint8_t adsrphase;

  uint8_t arpmode;
  uint8_t vibrate;
  uint8_t arpspeed;
  uint8_t kick;
  uint8_t arpstep;
  uint8_t overdrive;

  uint32_t count;
  uint32_t duration;

  uint16_t output;
  uint32_t cinc;

  uint16_t attack;
  uint16_t decay;
  uint16_t sustain;
  uint16_t release;
  uint16_t adsrvol;

  int32_t pitchbend;
  int32_t bendrate;
  int32_t maxbend;
};

#endif
