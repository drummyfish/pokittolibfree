#ifndef SYSTEM_LPC11U6X_H
#define SYSTEM_LPC11U6X_H

/*
  This file has been rewritten from scratch by Drummfish and is placed in
  public domain under CC0 1.0 waiver.
*/

#ifdef __cplusplus // for the linker
extern "C"
{
#endif

extern uint32_t SystemCoreClock;     /*!< System Clock Frequency (Core Clock)  */

extern void initSystem();

#ifdef __cplusplus // extern "C"
}
#endif

#endif // guard
