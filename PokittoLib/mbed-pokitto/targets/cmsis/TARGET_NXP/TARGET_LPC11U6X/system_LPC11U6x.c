/*
  This file has been rewritten from scratch by Drummyfish and is put in
  public domain under CC0 1.0 waiver.
*/

#include <stdint.h>
#include "LPC11U6x.h"
#include "system_LPC11U6x.h"

uint32_t SystemCoreClock = 48000000;

#define LPCSysConfig ((LPC_SYSCON_Type *) 0x40048000UL)

static void startUp(uint32_t value)
{
  volatile uint32_t helper;
  helper = LPCSysConfig->PDRUNCFG & 0x000025FFL;
  helper = helper & ~(value & 0x000025FFL);
  LPCSysConfig->PDRUNCFG = (helper | 0x0000C800L);
}

static void shutDown(uint32_t value)
{
  volatile uint32_t helper;
  helper = LPCSysConfig->PDRUNCFG & 0x000025FFL;
  helper = helper | (value & 0x000025FFL);
  LPCSysConfig->PDRUNCFG = (helper | 0x0000C800L);
}

void initSystem()
{
  LPCSysConfig->SYSAHBCLKCTRL |= 65536;
  LPCSysConfig->SYSPLLCTRL = 35;

  LPC_IOCON_Type *ptr = (LPC_IOCON_Type *) 0x40044000UL;

  ptr->PIO2_0 = 0x01;
  ptr->PIO2_1 = 0x01;

  LPCSysConfig->SYSOSCCTRL = 0;
  startUp(32);

  volatile uint32_t n;

  for (n = 0; n < 2500; ++n)
  {
    __NOP();
  }

  LPCSysConfig->SYSPLLCLKSEL = 1;
  LPCSysConfig->SYSPLLCLKUEN = 1;

  while (!(LPCSysConfig->SYSPLLCLKUEN & 0x01))
  {
  }

  shutDown(128);
  LPCSysConfig->SYSPLLCTRL = 0x00000023;
  startUp(128);

  while (!(LPCSysConfig->SYSPLLSTAT & 1))
  {
  }

  LPCSysConfig->MAINCLKSEL = 3;
  LPCSysConfig->MAINCLKUEN = 1;

  while (!(LPCSysConfig->MAINCLKUEN & 1))
  {
  }

  LPCSysConfig->SYSAHBCLKDIV = 0x00000001;
  LPCSysConfig->PDRUNCFG &= ~(1024);

  LPCSysConfig->PDRUNCFG &= ~(256);
  LPCSysConfig->USBPLLCLKUEN = 1;
  LPCSysConfig->USBPLLCLKSEL = 1;

  while (!(LPCSysConfig->USBPLLCLKUEN & 1))
  {
  }

  LPCSysConfig->USBPLLCTRL = 35;

  while (!(LPCSysConfig->USBPLLSTAT & 1))
  {
  }

  LPCSysConfig->USBCLKDIV = 1;
  LPCSysConfig->USBCLKSEL = 0;
}
