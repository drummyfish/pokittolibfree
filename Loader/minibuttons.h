#ifndef MINIBUTTONS_H
#define MINIBUTTONS_H

/*
  This file has been rewritten from scratch by Drummyfish and is placed in
  public domain under CC0 1.0 waiver.
*/

extern void          pokInitButtons();
extern void          pokPollButtons();
extern void          pokDeleteButtons();

extern unsigned char leftBtn();
extern unsigned char upBtn();
extern unsigned char rightBtn();
extern unsigned char downBtn();

extern unsigned char aBtn();
extern unsigned char bBtn();
extern unsigned char cBtn();

extern               DigitalIn ABtn;
extern               DigitalIn BBtn;
extern               DigitalIn CBtn;
extern               DigitalIn UBtn;
extern               DigitalIn DBtn;
extern               DigitalIn LBtn;
extern               DigitalIn RBtn;

extern volatile unsigned char heldStates[];

#define POK_BTN_LEFT_PIN  P1_25
#define POK_BTN_UP_PIN    P1_13
#define POK_BTN_RIGHT_PIN P1_7
#define POK_BTN_DOWN_PIN  P1_3

#define POK_BTN_A_PIN     P1_9
#define POK_BTN_B_PIN     P1_4
#define POK_BTN_C_PIN     P1_10

#define BTN_LEFT  0x00
#define BTN_UP    0x01
#define BTN_RIGHT 0x02
#define BTN_DOWN  0x03

#define BTN_A     0x04
#define BTN_B     0x05
#define BTN_C     0x06

#define NUM_BTN   7 // for the count

#define UPBIT     0x00
#define DOWNBIT   0x01
#define LEFTBIT   0x02
#define RIGHTBIT  0x03
#define ABIT      0x04
#define BBIT      0x05
#define CBIT      0x06

#endif // guard
