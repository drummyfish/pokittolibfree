#ifndef MINI_LOAD_H
#define MINI_LOAD_H

/*
  This file has been rewritten by Drummyfish and is placed in public domain
  under CC0 1.0 waiver.
*/

#include <mbed.h>
#include "Pokitto_settings.h"
#include "PokittoDisk.h"
#include "miniGFX.h"
#include "minibuttons.h"

#define CORNER_LUP     0x41
#define BAR_VERT       0x42
#define CORNER_LDOWN   0x43
#define BAR_HORIZ      0x44
#define CORNER_RDOWN   0x45
#define CORNER_RUP     0x46
#define DASH_VERT      0x47
#define HATCH_DIAG     0x48

#define APPL_ADDRESS   0x00020000
#define LOADER_ADDRESS 0x00039000
#define IAP_CODE_START 0x00030001

extern const unsigned char gfxC64[];
extern const unsigned char fontC64[];

extern void start_application(unsigned long address);
extern void execute_code(unsigned int address);
extern void m_initLCD();
extern void m_lcdClear();

#define LCD_RES_PIN    0x00
#define LCD_RES_PORT   0x01

#define LCD_RD_PORT    0x01

#define LCD_CD_PORT    0x00
#define LCD_WR_PORT    0x01
#define LCD_CD_PIN     0x02

#define LCD_WR_PIN     0x0C
#define LCD_RD_PIN     0x18

#define SET_RESET\
  {\
    LPC_GPIO_PORT->SET[LCD_RES_PORT] = 0x01 << LCD_RES_PIN;\
  }

#define CLR_RESET\
  {\
    LPC_GPIO_PORT->CLR[LCD_RES_PORT] = 0x01 << LCD_RES_PIN;\
  }

#define SET_CD\
  {\
    LPC_GPIO_PORT->SET[LCD_CD_PORT] = 0x01 << LCD_CD_PIN;\
  }

#define CLR_CD\
  {\
    LPC_GPIO_PORT->CLR[LCD_CD_PORT] = 0x01 << LCD_CD_PIN;\
  }

#define SET_WR\
  {\
    LPC_GPIO_PORT->SET[LCD_WR_PORT] = 0x01 << LCD_WR_PIN;\
  }

#define CLR_WR\
  {\
    LPC_GPIO_PORT->CLR[LCD_WR_PORT] = 0x01 << LCD_WR_PIN; __asm("nop");\
  }

#define SET_RD\
  {\
    LPC_GPIO_PORT->SET[LCD_RD_PORT] = 0x01 << LCD_RD_PIN;\
  }

#define CLR_RD\
  {\
    LPC_GPIO_PORT->CLR[LCD_RD_PORT] = 0x01 << LCD_RD_PIN;\
  }

#define SET_MASK_P2\
{\
  LPC_GPIO_PORT->MASK[2] = ~(0x7FFF8);\
}

#define CLR_MASK_P2\
{\
  LPC_GPIO_PORT->MASK[2] = 0;\
}

#define SET_CS              {                         }
#define CLR_CS              {                         }
#define CLR_CS_CD_SET_RD_WR { CLR_CD; SET_RD; SET_WR; }
#define CLR_CS_SET_CD_RD_WR { SET_CD; SET_RD; SET_WR; }
#define SET_CD_RD_WR        { SET_CD; SET_RD; SET_WR; }
#define SET_WR_CS           { SET_WR;                 }

#endif // guard
