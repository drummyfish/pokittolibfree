# author: Drummyfish
# license: CC0 1.0

in_file = open("BUILD/Loader.bin","rb")
data = in_file.read()
in_file.close()

res = "  "

count = 1

for d in data:
  res += "{0:#0{1}x}".format(ord(d),4) + ","

  if count % 12 == 0:
    res += "\n  "

  count += 1

res = res[:-1]

print(res)
