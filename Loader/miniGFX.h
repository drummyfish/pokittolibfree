#ifndef MINI_GFX_H
#define MINI_GFX_H

/*
  This file has been rewritten by Drummyfish and is placed in public domain
  under CC0 1.0 waiver.
*/

#include "miniprint.h"

extern void         pokDirectPixel(int x,int y, unsigned int color);
extern void         pokDirectRectangle(int x,int y, int w, int h,
                      unsigned int color);
extern void         pokDirectBitmap(int x, int y, const unsigned char *bitmap,
                      unsigned char a, unsigned char b);
extern void         pokDirectChar(int x, int y, const unsigned char *c,
                      unsigned int a);
extern void         pokDirectColumn(int x,int y, int l, unsigned int color);
extern void         pokDirectRow(int x,int y, int l, unsigned int color);

extern              int pok_scale;
extern              int pok_depth;
extern unsigned int pok_directcolor;
extern unsigned int palette[];

#define DC_BLACK    0x0000 //black
#define DC_WHITE    0xFFFF //white
#define DC_GREEN    0x07E0 //green
#define DC_CYAN     0x07FF //cyan
#define DC_MAGENTA  0xF81F //magenta
#define DC_YELLOW   0xFFE0 //yellow
#define DC_ORANGE   0xFC00 //orange
#define DC_RED      0xF800 //red
#define DC_DGREEN   0x0400 //dark green
#define DC_DCYAN    0x0410 //dark cyan
#define DC_DMAGENTA 0x8010 //dark magenta
#define DC_GRAY     0x7BEF //gray
#define DC_BLUE     0x001F //blue
#define DC_DBLUE    0x0010 //dark blue
#define DC_PINK     0xF810 //pink
#define DC_VIOLET   0x801F //violet

#define NONE        0x00
#define GB_STRETCH  0x01
#define X2          0x02
#define X3          0x03
#define AB_STRETCH  0x04

#define UNBUFFERED  0x00

#define BPP1        0x01
#define BPP2        0x02
#define BPP3        0x03
#define BPP4        0x04
#define BPP5        0x05

#endif // guard
