#!/bin/bash

# Upload firmware.bin to Pokitto.
# author: Miloslav Ciz
# license: CC0 1.0

echo "uploading..."

dd bs=1024 conv=nocreat,notrunc of="/media/$USER/CRP DISABLD/firmware.bin" if="$1"

echo "done"
